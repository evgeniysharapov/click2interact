source 'https://rubygems.org'
gem 'rails', '3.2.14'
gem 'rack', '1.4.5'
gem 'sqlite3', :group => [:development, :production]
gem 'mysql2', :group => :production

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem "bootstrap-sass", :github => 'evgeniysharapov/bootstrap-sass'
  gem 'jquery-rails'
  gem 'bootstrap-datepicker-rails'
  gem 'jquery-datatables-rails'
  gem 'compass-rails'
  gem 'font-awesome-sass-rails'
end

group :production do
  # we need it to compile assets
  gem 'libv8', '~> 3.11.8'
  gem 'therubyracer', :platforms => :ruby
end

gem "rspec-rails", ">= 2.11.0", :group => [:development, :test]
gem "factory_girl_rails", ">= 4.0.0", :group => [:development, :test]

group :development do
  gem 'guard'
  gem 'guard-rspec'
  gem 'guard-bundler'
  gem 'guard-cucumber'
  gem 'debugger' , :require => 'ruby-debug'
  gem 'pry-rails'
  gem 'capistrano'
  gem 'rvm-capistrano'
  gem 'better_errors'
  gem 'binding_of_caller'
end

group :test, :darwin do
  gem 'rb-fsevent'
  gem 'growl' # also install growlnotify from the Extras/growlnotify/growlnotify.pkg in Growl disk image
end

group :test do
  gem "capybara", ">= 1.1.2"
  gem "poltergeist"
  gem "email_spec", ">= 1.2.1"
  gem "cucumber-rails", ">= 1.3.0", :require => false
  # 1.1.1 fails with postresql adapter error message
  gem "database_cleaner", "~> 0.9.1"
  gem "launchy", ">= 2.1.2"
end

gem "devise", ">= 2.1.2"
gem "cancan", ">= 1.6.8"
gem "rolify", ">= 3.2.0"
gem "fog"
gem 'paperclip', github: 'thoughtbot/paperclip'
gem 'draper'
gem 'simple_form'
gem 'cocoon'
gem 'haml-rails'
gem 'mailboxer'
gem 'roo'
gem 'unicorn'

