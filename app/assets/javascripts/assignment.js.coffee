# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.Application ||= {}
# This function calls Assginment controller and returns list
# of assignees based on the Assignment type (either Group or User)
# Application.get_assignee_list = (type) ->
#  $.ajax
#    url: "#{update_artists_path}"
#    data:
#      genre_id: $("#genres_select").val()
#    dataType: "script"

jQuery ->
        $('#assignments_list').dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [ 0, 6, 7] }
            ],
          "sPaginationType": "bootstrap"

