# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.Application ||= {}

jQuery ->
        $('#groups').dataTable
         "bPaginate" : false,
         "aoColumns": [null,
                "bSortable": false],
         "sPaginationType": "bootstrap",
         "oLanguage":
                      "sSearch": ""

