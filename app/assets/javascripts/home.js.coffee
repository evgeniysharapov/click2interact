# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.Application ||= {}

jQuery ->
        $('#home_page_announcements').dataTable
          "sDom": "<'row-fluid' <'span6' <'pull-left'p>><'span6'f>r>t<'row-fluid'<'span6' <'pull-left'p>>>",
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""
jQuery ->
        $('#home_page_reports').dataTable
          "sDom": "<'row-fluid' <'span6.offset6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "sPaginationType": "bootstrap",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [ 2] }
            ],
          "oLanguage":
                "sSearch": ""

jQuery ->
        $("#home_page_report_submissions").dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [ 2] }
            ],
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""

jQuery ->
        $("#home_page_conversations").dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [] }
            ],
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""

# activating tables for the form Tab
jQuery ->
        $(".home_page_forms").dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [3] }
          ],
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""

# add bootstrap classes
jQuery ->
        $('.dataTables_filter input').addClass('input-medium search-query')



