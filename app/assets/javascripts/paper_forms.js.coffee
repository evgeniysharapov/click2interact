# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.Application ||= {}

Application.show_choice_controls = (selector) ->
  if $(selector).val() == 'choice'
     $(selector).parents('.select').siblings('.add-reply-action').css('visibility','visible')
  else
     $(selector).parents('.select').siblings('.add-reply-action').css('visibility','hidden')

jQuery ->
        $('#paper_forms').dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [3, 5] }
            ],
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""

# add bootstrap classes
jQuery ->
        $('.dataTables_filter input').addClass('input-medium search-query')

