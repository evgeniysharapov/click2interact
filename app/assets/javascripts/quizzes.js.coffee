# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.Application ||= {}

jQuery ->
        $('#quizzes_index').dataTable
          "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
          "aoColumnDefs": [
              { "bSortable": false, "aTargets": [0, 3, 5] }
            ],
          "sPaginationType": "bootstrap",
          "oLanguage":
                "sSearch": ""

jQuery ->
        $('.dataTables_filter input').addClass('input-medium search-query')
