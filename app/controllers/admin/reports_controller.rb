class Admin::ReportsController < ApplicationController
  layout "admin"
  before_filter :authenticate_user!
  before_filter :set_active

  load_and_authorize_resource

  # lists available reports
  def index
    @reports = Report.all
  end

  # TODO: make showing the report template meaningful
  def show
    redirect_to action: :index
  end

  # shows form for uploading report script
  def new
    @report = Report.new
  end

  def create
    src = params[:source]
    parser = Click::Reports::Parser.new

    begin
      parser.load src
      @report = parser.parsed
      if @report.save
        redirect_to admin_reports_path, notice: "Report #{@report.title} was successfully created."
      else
        render action: "new"
      end
    rescue StandardError
      flash[:error] = 'Could not parse report script.'
      @source = src
      render action: "new"
    end

  end

  def destroy
    @report = Report.find params[:id]
    @report.destroy
    redirect_to admin_reports_path, notice: "Report #{@report.title} was successfully deleted."
  end

  def edit
    @report = Report.find params[:id]
    parser = Click::Reports::Parser.new
    begin
      @source = parser.unparse @report
      render action: "new"
    rescue StandardError
      flash[:error] = 'Could not parse report script.'
      @source = src
      render action: "new"
    end
    
  end

  def update
  end

  private
  def set_active
    @active = 'reports'
  end

end
