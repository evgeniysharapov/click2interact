class Admin::UserImportsController < ApplicationController
  def new
    @user_import = UserImport.new
  end

  def create
    @user_import = UserImport.new(params[:user_import])
    if @user_import.save
      redirect_to admin_users_url, notice: "Imported #{@user_import.imported_users.size} users successfully."
    else
      render :new
    end
  end
end
