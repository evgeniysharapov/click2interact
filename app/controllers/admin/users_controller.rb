class Admin::UsersController < BasicUserController
  layout "admin"
  before_filter :authenticate_user!
  before_filter :set_active

  load_and_authorize_resource

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end

  def show
    redirect_to action: "edit"
  end

  def edit
    @user = User.find(params[:id])
    # if no profile, create it
    @user.build_profile if @user.profile.nil?
  end

  def create
    # TODO: send email notification with confirmation link to the user
    @user = User.new(params[:user].merge({password: 'password', password_confirmation: 'password'}))     # add default passwords
    if @user.save
      @user.change_groups_to params[:groups] if params[:groups]
      @user.change_roles_to params[:roles] if params[:roles]
      redirect_to edit_admin_user_path(@user), notice: 'User was successfully created.'
    else
      render action: "new"
    end
  end

  def new
    @user = User.new
    @user.build_profile
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "User #{@user.name} was deleted!"
    redirect_to action: 'index'
  end

  # This method approves user from the admin view
  def approve
    @user = User.find(params[:id])
    @user.approved = true
    @user.save
    flash[:notice] = "User #{@user.name} was approved!"
    redirect_to action: 'index'
  end

  # Disapproves user (he can't log in anymore, but all its data is intact)
  def disapprove
    @user = User.find(params[:id])
    @user.approved = false
    @user.save
    flash[:notice] = "User #{@user.name} was disapproved!"
    redirect_to action: 'index'
  end

  def notify
    user = User.find(params[:id])
    unless user.nil?
      if user.send_confirmation_instructions
        flash[:notice] = "Notification to user #{user.name} was emailed!"
        redirect_to action: 'index'
      else
        flash[:error] = "Could not email notification to user #{user.name}!"
        redirect_to action: 'index'
      end
    end
  end

  # Updates the user roles and groups and whatever else is there on the Admin panel
  def change
    @user = User.find params[:user_id]
    if @user.update_attributes params[:user]
      @user.change_groups_to params[:groups] if params[:groups]
      @user.change_roles_to params[:roles] if params[:roles]
      redirect_to edit_admin_user_path(@user), notice: "User #{@user.name} was updated!"
    else
      render action: 'edit'
    end
  end

  private
  def set_active
    @active = 'users'
  end

end
