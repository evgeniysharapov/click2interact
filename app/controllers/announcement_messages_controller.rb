class AnnouncementMessagesController < ApplicationController
  before_filter :authenticate_user!
  # load_and_authorize_resource

  # Lists all announcements from the current_user
  def index
    if params[:user_id]
      # Renders announcements from the :user_id to the current_user
      @author = User.find(params[:user_id])
      @announcements = AnnouncementMessage.joins(:announcements).where(announcements: { recipient_id: current_user}, author_id: @author)
    else
      @announcements = AnnouncementMessage.where(author_id: current_user)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @announcements }
    end
  end

  def recipients
    @announcement_recipients = AnnouncementMessage.find(params[:id]).announcements
  end

  # gets past announcements for the current user
  def past
    @announcements = AnnouncementMessage.joins(:announcements).where(announcements: { recipient_id: current_user, showable: false}).all
  end

  def show
    @announcement = AnnouncementMessage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @announcement }
    end
  end

  def new
    @announcement = AnnouncementMessage.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @announcement }
    end
  end

  def create
    # TODO: take it out to a method or a model 
    recipient_type = params[:announcement_message][:recipient_type]
    recipient = eval("#{recipient_type}.find(params[:announcement_message][:recipient])")
    params[:announcement_message][:author] = current_user
    params[:announcement_message].delete(:recipient)
    params[:announcement_message].delete(:recipient_type)
    @announcement = AnnouncementMessage.new(params[:announcement_message]).announce_to(recipient)
    # TODO: make controller decide what to do more elegantly
    if @announcement.save
      redirect_to @announcement, notice: 'Announcement was successfully created.'
    else
     render action: "new"
    end
  end

  def update
    @announcement = AnnouncementMessage.find(params[:id])
    respond_to do |format|
      if @announcement.update_attributes(params[:announcement])
        format.html { redirect_to @announcement, notice: 'Announcement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @announcement = AnnouncementMessage.find(params[:id])
    @announcement.destroy
    respond_to do |format|
      format.html { redirect_to announcement_messages_url }
      format.json { head :no_content }
    end
  end

  def unshow
    announcement = Announcement.where(message_id: params[:announcement_message_id], recipient_id: current_user ).first
    announcement.update_attribute :showable, false
    respond_to do |format|
      format.js
    end
  end
  # makes an announcement showable to the users again. Technically opposite of the unshow
  def resend
    if params[:user_id]
      # should be resend to a particular user
      announcements = Announcement.where(message_id: params[:announcement_message_id], recipient_id: params[:user_id])
    else
      # resend to all users
      announcements = Announcement.where(message_id: params[:id])
    end
    Announcement.transaction do
      announcements.each do |a|
        a.update_attribute :showable, true
      end
    end
    redirect_to :back
  end
end
