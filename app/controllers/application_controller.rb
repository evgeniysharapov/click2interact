class ApplicationController < ActionController::Base
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end
  # checks if collection of AR models has errors in it and returns true if
  # at none of them has an error
  def no_errors_in(coll)
    coll.map(&:errors).delete_if{|e| e.messages.empty?}.empty?
  end


  def plural_or_singular (count, singular)
    if singular == 'was'
      ((count == 1 || count =~ /^1(\.0+)?$/) ? 'was' : 'were')
    elsif singular == 'is'
      ((count == 1 || count =~ /^1(\.0+)?$/) ? 'is' : 'are')
    elsif singular == 'has'
      ((count == 1 || count =~ /^1(\.0+)?$/) ? 'has' : 'have')
    else
      ((count == 1 || count =~ /^1(\.0+)?$/) ? singular : singular.pluralize)
    end
  end

  # If user logged-in first time, then after successful login we will
  # forward user to his profile page to edit information about him
  def after_sign_in_path_for(resource)
    if resource.sign_in_count < 2
      edit_user_registration_path
    else
      root_path
    end
  end

end
