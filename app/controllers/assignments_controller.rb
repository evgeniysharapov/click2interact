class AssignmentsController < ApplicationController
  before_filter :load_assignable
  before_filter :authenticate_user!
  # TODO: make it working with recipient_type
  #load_and_authorize_resource

  # Lists assignments
  def index
    if @assignable
      # if there's an assignable item get a list of assignments for it
      @assignments = @assignable.assignments
    else
      # /users/:user_id/assignments
      # show all assignments for a user (just redirect to 'for' action)
      redirect_to action: :for
    end
  end

  # Returns all assignments for all assignables for a given user
  # TODO: check that current_user can see assignments for the user
  def for
    @user = User.find params[:user_id]
    @assignments = Assignment.for(@user)
    @assignments = @assignments.forms if params.has_key? :forms
    @assignments = @assignments.reports if params.has_key? :reports
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignments }
    end
  end

  # Returns all assignments created by a user for all types of assignables
  # TODO: check that current_user can see assignments by the user
  def by
    @user = User.find params[:user_id]
    @assignments = Assignment.by(@user)
    @assignments = @assignments.forms if params.has_key? :forms
    @assignments = @assignments.reports if params.has_key? :reports
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignments }
    end
  end

  # creates assignment or assignments depending on the +param[:assignment][recipient_type]+
  def create
    assignments = Assignment.create_for params[:assignment] do |assignment|
      assignment.sender = current_user
      assignment.assignable = @assignable
    end
    @assignment = assignments.first
    # TODO: make controller decide what to do more elegantly
    if no_errors_in assignments
      redirect_to @assignable, notice: "#{plural_or_singular(assignments.count,'assignment').capitalize} #{plural_or_singular(assignments.count, 'was')} successfully created."
    else
      render action: "new"
    end
  end

  def new
    @assignment = @assignable.assignments.new
    # TODO: figure out list of assignees based on the type of
    # assignment and current user
    @assignee_list = User.all
  end

  # TODO: make these methods work not only with paper_forms
  def take
    @assignment = Assignment.find params[:id]
    # TODO: what's below need to be generalized and put into a model
    if @assignable.is_a? PaperForm
      @response_set = ResponseSet.new user: current_user, paper_form: @assignable
      @response_set.paper_form.articles.each do |article|
        @response_set.responses.build article: article
      end
    elsif @assignable.is_a? Report
      # redirect to a teacher report controller
      redirect_to new_teacher_report_report_submission_path(@assignable)
    elsif @assignable.is_a? Quiz
      # TODO: create AnswerValues for all questions
      @quizTake = QuizTake.create_for_quiz @assignable
      @quizTake.user = current_user
      @quizTake.assignment = @assignment
      @quizTake.started_at = Time.now
      render 'quizzes/take'
    end

  end

  def complete
    @assignment = Assignment.find params[:id]
    if @assignable.is_a? PaperForm
      @response_set = ResponseSet.new(params[:response_set])
      @response_set.assignment = @assignment
      @response_set.paper_form = @assignable
      @response_set.user = current_user
      @response_set.complete! if @response_set.valid?
      if @response_set.save
        @response_set.assignment.save!
        redirect_to completed_paper_form_assignment_path(@assignable, @assignment), notice: 'Form was successfully completed.'
      else
        render  action: "show"
      end
    elsif @assignable.is_a? Quiz
      @quizTake = QuizTake.new(params[:quiz_take])
      @quizTake.quiz = @assignable
      @quizTake.assignment = @assignment
      @quizTake.user = current_user
      if @quizTake.save
        @quizTake.complete!
        @assignment.complete!
        redirect_to completed_quiz_assignment_path(@assignable, @assignment), notice: "Quiz was completed!"
      end
    end
  end

  def completed
    @assignment = Assignment.find params[:id]
    if @assignable.is_a? PaperForm
      @response_set = ResponseSet.find_by_assignment_id(@assignment)
    elsif @assignable.is_a? Quiz
      @quizTake = QuizTake.find_by_assignment_id(@assignment)
      render 'quizzes/completed'
    end
  end

  def destroy
    @assignment = Assignment.find params[:id]
    @assignment.destroy
    respond_to do |format|
      format.html { redirect_to action: :index }
      format.json { head :no_content }
    end
  end

  private
  def load_assignable
    @assignable  = if params[:paper_form_id]
                     PaperForm.find(params[:paper_form_id])
                   elsif params[:report_id]
                     Report.find params[:report_id]
                   elsif params[:quiz_id]
                     Quiz.find params[:quiz_id]
                   end
  end

end
