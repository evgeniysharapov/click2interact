require 'securerandom'

class BasicUserController < ApplicationController

  def new_child
    @user = User.new
    @user.add_role :child
    @parent = User.find params[:user_id]
  end

  def create_child
    @parent = User.find params[:user_id]
    @child = User.new(params[:user].merge({email: SecureRandom.hex(10) + '@click2interact.com', password: 'password', password_confirmation: 'password'}))     # add default passwords
    if @child.save
      @child.change_groups_to params[:groups] if params[:groups]
      @child.change_roles_to params[:roles] if params[:roles]
      @child.add_role :child
      @parent.children << @child
      flash[:notice] = 'Child was successfully created.'
      redirect_to action: :edit, id: @parent
    else
      render action: "new_child"
    end
  end
end
