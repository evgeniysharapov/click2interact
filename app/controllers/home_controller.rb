class HomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @users = User.all
    @announcements = (Announcement.where(recipient_id: current_user, showable: true)).map(&:message)
    @all_announcements = (Announcement.where(recipient_id: current_user)).map(&:message)
    @messages = current_user.mailbox.inbox.map(&:messages).flatten
    @form_assignments = FormAssignmentsPresenter.new(Assignment.for(current_user).forms)
    @quizz_assignments = Assignment.for(current_user).quizzes.where(completed_at: nil)
    @report_assignments = Assignment.for(current_user).reports.where(completed_at: nil)
    @report_submissions = ReportSubmission.for_children.of_parent(current_user).order('report_submissions.updated_at desc')
  end

  helper do
    def has_announcements?
      not @all_announcements.empty?
    end

    def has_quiz_assignments?
      not @quizz_assignments.empty?
    end

    def has_report_assignments?
      not @report_assignments.empty?
    end

    def has_report_submissions?
      not @report_submissions.empty?
    end

    def has_form_assignments?
      not @form_assignments.pending.empty?
    end

    def number_of_form_assignments
      @form_assignments.pending.count
    end

    def number_of_quiz_assignments
      @quizz_assignments.count
    end

    def number_of_report_assignments
      @report_assignments.count
    end

    def numbers_of_report_submissions
      @report_submissions.count
    end

    def has_new_conversations?
      not @messages.empty?
    end

    def number_of_new_conversations
      @messages.count
    end
  end

end
