class PaperFormsController < ApplicationController
  before_filter :authenticate_user!
  load_and_authorize_resource

  # GET /paper_forms
  # GET /paper_forms.json
  def index
    # restrict paperforms to only ones that have been created by the user
    @paper_forms = PaperForm.where(author_id: current_user)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @paper_forms }
    end
  end

  # GET /paper_forms/1
  # GET /paper_forms/1.json
  def show
    @paper_form = PaperForm.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @paper_form }
    end
  end

  # GET /paper_forms/new
  # GET /paper_forms/new.json
  def new
    @paper_form = PaperForm.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @paper_form }
    end
  end

  # GET /paper_forms/1/edit
  def edit
    @paper_form = PaperForm.find(params[:id])
  end

  # POST /paper_forms
  # POST /paper_forms.json
  def create
    @paper_form = PaperForm.new(params[:paper_form])
    @paper_form.author = current_user
    respond_to do |format|
      if @paper_form.save
        format.html { redirect_to @paper_form, notice: 'Paper form was successfully created.' }
        format.json { render json: @paper_form, status: :created, location: @paper_form }
      else
        format.html { render action: "new"}
        format.json { render json: @paper_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /paper_forms/1
  # PUT /paper_forms/1.json
  def update
    @paper_form = PaperForm.find(params[:id])
    respond_to do |format|
      if @paper_form.update_attributes(params[:paper_form])
        format.html { redirect_to @paper_form, notice: 'Paper form was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @paper_form.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paper_forms/1
  # DELETE /paper_forms/1.json
  def destroy
    @paper_form = PaperForm.find(params[:id])
    @paper_form.destroy
    respond_to do |format|
      format.html { redirect_to paper_forms_url }
      format.json { head :no_content }
    end
  end

  def possible_assignees
    if params[:assignment_type] == "Group"
      @assignee_list = Group.all
    elsif params[:assignment_type] == "User"
      @assignee_list = User.all
    else
      @assignee_list = []
    end
    #respond_with(@assignee_list)
    respond_to do |format|
      format.js
    end

  end

end
