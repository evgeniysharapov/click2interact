class Parent::ReportSubmissionsController < ApplicationController
  before_filter :authenticate_user!

  # shows all submission for all reports for all children
  # /parent/reports
  def index
    @report_submissions = ReportSubmission.for_children.of_parent(current_user)
  end

  def submissions
    @report = Report.find params[:report_id]
    @report_submissions = @report.report_submissions.submitted_by(current_user)
    if params[:ago]
      @report_submissions = @report_submissions.submitted_n_days_ago(params[:ago].to_i)
    end
  end

  def show
    @report = Report.find params[:report_id]
    @report_submission = ReportSubmission.find params[:id]
    @report_submission.organize_submission_entries
  end

end
