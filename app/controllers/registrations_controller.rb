class RegistrationsController < Devise::RegistrationsController
  # This method allows users to use PIN on their sign-up
  # PIN means that they have been pre-approved by the admin
  # If they entered email/PIN combination that matches our 
  # records, we forward them to the confirmation page that will ask
  # them to choose password
  def new_pin
    build_resource({}) if self.resource.nil?
    respond_with self.resource
  end


  def create
    if params[:user][:pin]
      # PINnable registration
      # TODO: should we use resource find instead of explicitly using user?
      resource = (User.where email: params[:user][:email], pin: params[:user][:pin]).first
      if resource
        if resource.active_for_authentication? 
          set_flash_message :notice, :signed_up if is_navigational_format?
          sign_up(resource_name, resource)
          respond_with resource, :location => after_sign_up_path_for(resource)
        end
      else
        # didn't find user with this email and pin, then return it back
        # with error and suggestion to use regular sign-up
        set_flash_message :error, :no_pin_found if is_navigational_format?
        build_resource(params[:user])
        # TODO: make form keep its parameters (email and pin)
        respond_with self.resource, location: users_sign_up_with_pin_path
        #render action: :new_pin
      end
    else
      super
    end
  end

  def update
    # required for settings form to submit when password is left blank
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(params[:user])
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end
end
