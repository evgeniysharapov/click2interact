class Teacher::ReportSubmissionsController < ApplicationController
  before_filter :authenticate_user!

  # /teacher/reports
  # Shows reports assigned for a submission
  def index
    @assignments = Assignment.for(current_user).reports
  end

  def submissions
    @report = Report.find params[:report_id]
    @report_submissions = @report.report_submissions.submitted_by(current_user)
    if params[:ago]
      @report_submissions = @report_submissions.submitted_n_days_ago(params[:ago].to_i)
    end
  end

  def show
    @report = Report.find params[:report_id]
    @report_submission = ReportSubmission.find params[:id]
    @report_submission.organize_submission_entries
  end


  def new
    @report = Report.find params[:report_id]
    @report_submission = ReportSubmission.new user: current_user, report: @report
    @report_submission.organize_submission_entries
  end

  def create
    @report = Report.find params[:report_id]
    @report_submission = ReportSubmission.new params[:report_submission].merge({report: @report, user: current_user})
    if @report_submission.save
      redirect_to teacher_report_report_submission_path(@report, @report_submission), notice: 'Report was successfully submitted.'
    else
      render "new", error: 'Some error during saving the report.'
    end
  end

  def destroy
    @report =  Report.find params[:report_id]
    @report_submission = ReportSubmission.find params[:id]
    @report_submission.destroy unless @report_submission.nil?
    redirect_to action: :submissions
  end

end
