class TextMessagesController < ApplicationController
  before_filter :authenticate_user!
  #load_and_authorize_resource

  def index
    # TODO make it using sender
    @sms_list = TextMessage.where( :user_id => current_user)
  end

  def destroy
    @sms = TextMessage.find(params[:id])
    @sms.destroy
    redirect_to :action => :index
  end

  def show
  end

  def new
  end

  def create
    text_messages = TextMessage.create_for params[:text_message] do |sms|
      sms.sender = current_user
    end
    @sms = text_messages.first
    # TODO: consider moving it into after save on model.
    errors = @sms.send_text
    if errors.empty?
      flash[:notice] = "Text message has been sent."
      redirect_to text_messages_path
    else
      flash[:notice] = "Text message has been sent."
      flash[:error] = errors
      render :new
    end
  end

end
