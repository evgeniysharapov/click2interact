class UsersController < BasicUserController
  before_filter :authenticate_user!
  #load_and_authorize_resource

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end
  # Updates the user roles and groups and whatever else is there on the Admin pane

  def edit
    redirect_to edit_user_registration_path
  end

  def change
    @user = User.find params[:user_id]
    @user.change_groups_to params[:groups]
    @user.change_roles_to params[:roles]
    redirect_to @user, notice: "User #{@user.name} was updated!"
  end

  # Returns list of users or groups that coule be send a message, announcement
  # and alike by current_user
  def recipient_list
    @recipient_list = current_user.find_recipients params[:recipient_type]
    respond_to do |format|
      format.js
    end
  end

end
