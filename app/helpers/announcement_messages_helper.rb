module AnnouncementMessagesHelper
  # Returns string that has all of the  recipients names in it
  # separated by commas. Useful in "Sent To: ..." lines.
  def recipients(announcement_message)
    announcement_message.announcements.map(&:recipient).map(&:name).join(", ")
  end
  # if only one recipient returns its name, otherwise returns links to the
  # page that has announcements listed
  def link_to_recipients_if_many_or_name(announcement_message)
    if announcement_message.recipients.size == 1
      announcement_message.recipients.first.name
    else
      link_to "Recipients", recipients_announcement_message_path(announcement_message)
    end
  end

  def display_date(input_date)
    input_date.strftime("%d %B %Y at %I:%M%p")
  end

  #
  # This method makes sure that we are not going to break if there's
  # some symbol that is not in UTF-8
  def content(announcement_message)
    announcement_message.content.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
  end

end
