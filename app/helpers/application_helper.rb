module ApplicationHelper
  def btn_link_to *args
    if args.last.class == Hash
      # already have hash arguments, just add class to the hash
      args.last[:class]="btn"
      link_to *args
    else
      link_to *args, :class => "btn"
    end
  end

  # add methods for different buttons in Twitter bootstrap
  %w(danger info primary success warning inverse).each do |btn_type|
    define_method ("btn_" + btn_type + "_link_to" ).to_sym do |*args|
      if args.last.class == Hash
        # already have hash arguments, just add class to the hash
        args.last[:class]="btn btn-" + btn_type
        link_to *args
      else
        link_to *args, :class => "btn btn-" + btn_type
      end
    end
  end

  def list_of_roles (user)
    user.roles.map(&:name).join ", "
  end

  def list_of_groups (user)
    user.groups.map(&:name).join ", "
  end

  def role_css_class(user)
    if user
      "role-" + user.roles.first.name unless user.roles.empty?
    end
  end

  def nav_active_if ( condition, classes="", &block )
    if condition
      content_tag :li, class: 'active ' + classes do
        block.call
      end
    else
      content_tag :li, class: classes do
        block.call
      end
    end
  end

end
