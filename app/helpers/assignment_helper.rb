module AssignmentHelper
  def assignment_status(assignment)
    assignment.completed_at.nil? ? "Pending" : "Completed"
  end

  def can_be_taken?(assignment)
    assignment.completed_at.nil?
  end

  # returns url path before the question mark
  def path_before_question_mark
    request.fullpath.match(/[^?]+/).to_s
  end

end
