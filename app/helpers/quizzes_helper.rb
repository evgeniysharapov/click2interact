module QuizzesHelper
  # Return Complete if all assignments for the quiz have been
  # completed and Pending otherwise
  def quiz_assignments_status(quiz)
    if quiz.assignments.where(completed_at: nil).empty?
      "Complete"
    else
      "Pending"
    end
  end
end
