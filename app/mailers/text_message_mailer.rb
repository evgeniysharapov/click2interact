class TextMessageMailer < ActionMailer::Base
  default from: "notifications@click2interact.com"

  CARRIER_EMAILS = {
    "AT&T" => "@txt.att.net",
    "Qwest" => "@qwestmp.com",
    "T-Mobile" => "@tmomail.net",
    "Verizon" => "@vtext.com",
    "Sprint" => "@pm.sprint.com",
    "Virgin Mobile" => "@vmobl.com",
    "Nextel" => "@messaging.nextel.com",
    "Alltel" => "@message.alltel.com",
    "Metro PCS" => "@mymetropcs.com",
    "Powertel" => "@ptel.com",
    "Boost Mobile" => "@myboostmobile.com",
    "Suncom" => "@tms.suncom.com",
    "Tracfone" => "@mmst5.tracfone.com",
    "U.S. Cellular" => "@email.uscc.net"
  }

  def text_message_email(from_name, phone, carrier, content)
    @from_name = from_name
    @body = content
    email_address = CARRIER_EMAILS[carrier]
    mail(:to => phone.to_s + email_address)
  end

end
