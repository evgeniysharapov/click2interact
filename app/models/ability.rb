class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user || User.new # guest user (not logged in)

    # call different method for each role
    # we use Role class
    @user.roles.each { |role| send(role.name.to_sym) }

    # if user doesn't have any role
    if @user.roles.size == 0
      can :read, :all
    end
  end

  ### Students
  def student
    can :read, :all
    can :complete, Assignment
    can :receive, Quiz
    can :take, PaperForm
  end

  ### Parents
  def parent
    can :complete, Assignment
    can :receive, Quiz
    can :read, :all
    can :view, ReportSubmission
    can :take, PaperForm
    can :fill, PaperForm
  end

  ### Teachers
  def teacher
    can :manage, Assignment
    can :manage, PaperForm
    can :manage, TextMessage
    can :manage, Group
    can :manage, Announcement
    can :create, AnnouncementMessage
    can :delete, AnnouncementMessage
    can :resend, AnnouncementMessage
    can :read, :all
    cannot :receive, Quiz
    can :manage, Quiz
    can :submit, ReportSubmission
    can :create, ReportSubmission
    can :take, PaperForm
    can :manage, :children
  end

  ### Administrators
  def admin
    can :manage, :all
  end

end
