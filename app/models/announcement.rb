# == Schema Information
#
# Table name: announcements
#
#  id           :integer          not null, primary key
#  content      :text
#  recipient_id :integer
#  sender_id    :integer
#  showable     :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Announcement < ActiveRecord::Base
  include Click::Sendable

  attr_accessible :showable, :message

  belongs_to :message, class_name: AnnouncementMessage, inverse_of: :announcements

  validates_presence_of :message

  before_save :update_sender

  private

  # Sender is essentially an author of the associated
  # AnnouncementMessage
  def update_sender
    sender = message.author
  end

end
