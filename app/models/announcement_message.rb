class AnnouncementMessage < ActiveRecord::Base
  attr_accessible :author, :content, :group, :attachment, :link

  has_attached_file :attachment

  belongs_to :author, class_name: 'User'
  belongs_to :group

  validates_presence_of :author, :content

  has_many :announcements, dependent: :destroy, foreign_key: 'message_id'
  # at least one association should be there
  validates :announcements, length: { minimum: 1 }
  # if group? then has exactly group.user_size announcements
  # see {GroupSizeValidator#validates_each}
  validates :announcements, group_size: true

  # shortcut method that creates announcements records
  # returns AnnouncementMessage object itself which is
  # good to use AnnoucementMessage.new(...).announce_to(...)
  def announce_to(whom)
    if whom.is_a? User
      recipient_type = 'User'
    elsif whom.is_a? Group
      recipient_type = 'Group'
    end
    unless recipient_type.nil?
      a_list = Announcement.create_for recipient_type: recipient_type, recipient: whom do |a|
        a.sender = author
        a.message = self
      end
      a_list.each do |a|
        announcements << a
      end
    end
    self
  end

  def recipients
    announcements.map(&:recipient)
  end

end
