class AnswerValue < ActiveRecord::Base
  attr_accessible :question, :answer_id, :question_id
  belongs_to :question
  belongs_to :answer
  belongs_to :quiz_take
end
