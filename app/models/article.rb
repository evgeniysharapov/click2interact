# == Schema Information
#
# Table name: articles
#
#  id            :integer          not null, primary key
#  paper_form_id :integer
#  content       :text
#  response_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Article < ActiveRecord::Base
  attr_accessible :content, :replies_attributes, :response_type
  belongs_to :paper_form, inverse_of: :articles

  validates :content, presence: true
  validates :paper_form, presence: true
  validates :response_type, inclusion: { in: %w(choice integer float string) }

  has_many :replies, dependent: :destroy
  accepts_nested_attributes_for :replies, reject_if: :all_blank

  before_validation :default_values

  def default_values
    self.response_type ||= 'string'
  end

end
