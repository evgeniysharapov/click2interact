# == Schema Information
#
# Table name: assignments
#
#  id              :integer          not null, primary key
#  sender_id       :integer
#  recipient_id    :integer
#  due_date        :date
#  completed_at    :datetime
#  assignable_id   :integer
#  assignable_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Assignment < ActiveRecord::Base
  include Click::Sendable

  attr_accessible :completed_at, :due_date, :assignable

  belongs_to :assignable, polymorphic: true
  has_many :response_sets, inverse_of: :assignment, dependent: :destroy

  validates :due_date, presence: true

  scope :for , lambda { |u| where(recipient_id: u) }
  scope :by , lambda { |u| where(sender_id: u) }

  scope :of_type, lambda{|t| where(assignable_type: t.name) }
  scope :reports, of_type(Report)
  scope :forms, of_type(PaperForm)
  scope :quizzes, of_type(Quiz)

  def complete!
    self.update_attributes completed_at: Time.now
  end

end
