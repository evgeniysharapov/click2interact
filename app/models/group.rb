# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Group < ActiveRecord::Base
  attr_accessible :name
  has_and_belongs_to_many :users
  belongs_to :owner, class_name: 'User'

  attr_accessible :possible_members, :group_members, :button_action, :user_ids

  attr_writer :button_action, :possible_members

  validates :name, presence: true
  validates :owner, presence: true
  
  def possible_members
    User.all - group_members
  end

  def group_members
    self.users
  end

end
