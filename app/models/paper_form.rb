# == Schema Information
#
# Table name: paper_forms
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  author_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PaperForm < ActiveRecord::Base
  attr_accessible :title, :articles_attributes

  belongs_to :author, class_name: 'User', foreign_key:  :author_id
  has_many :articles, dependent: :destroy, inverse_of: :paper_form

  validates :author, presence: true
  validates :title, presence: true

  accepts_nested_attributes_for :articles

  has_many :assignments, :as => :assignable

  has_many :response_sets

  # Returns Completed when all assignments for the Form have been
  # completed and Pending when some assignments are pending
  def assignments_completed?
    if assignments.all? {|a| not a.completed_at.nil? }
      "Complete"
    else
      "Pending"
    end
  end
end
