class Parenting < ActiveRecord::Base
  belongs_to :parent, class_name: 'User'
  belongs_to :child, class_name: 'User'
  attr_accessible :parent, :child
end
