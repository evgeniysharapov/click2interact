# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  phone      :string(255)
#  carrier    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Profile < ActiveRecord::Base
  include Click::Carrier

  attr_accessible :carrier, :phone, :user_id, :bio, :dob, :surname

  belongs_to :user

  before_validation :find_out_carrier

  validate :validates_phone_number, :validates_carrier

  def find_out_carrier
    begin
      self.carrier ||= find_carrier(phone)
    rescue => e
      errors.add(:phone, "#{e}")
    end
  end

  def validates_phone_number
    if valid_phone_number(phone).nil?
      errors.add(:phone, "is not a valid phone number")
    end
  end

  def validates_carrier
    if carrier.nil?
      errors.add(:phone, "belongs to unknown carrier")
    end
  end
end
