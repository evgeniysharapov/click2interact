# == Schema Information
#
# Table name: quizzes
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Quiz < ActiveRecord::Base
  attr_accessible :title, :questions_attributes
  has_many :questions, :dependent => :destroy
  accepts_nested_attributes_for :questions, :allow_destroy => true, :reject_if => lambda {|a| a[:content].blank? }
  validates :title, presence: true
  has_many :assignments, :as => :assignable

end
