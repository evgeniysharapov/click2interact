class QuizTake < ActiveRecord::Base
  attr_accessible :quiz, :completed_at, :completed, :started_at
  belongs_to :user
  belongs_to :assignment
  belongs_to :quiz
  has_many :answers, dependent: :destroy, class_name: "AnswerValue"

  accepts_nested_attributes_for :answers, allow_destroy: true
  attr_accessible :answers_attributes

  # creates new answer_value_set for a given quiz
  def self.create_for_quiz(quiz)
    qt = QuizTake.new quiz: quiz
    quiz.questions.each do |question|
      qt.answers.build question: question
    end
    qt
  end

  def complete!
    self.update_attributes completed_at: Time.now, completed: true
  end

  # Returns string N/M where N is number of correct answers and M is
  # number of questions in the quiz.
  def score
    if completed?
      correct = self.answers.joins(:answer).where(answers: { correct: true}).count
      "#{correct}/#{quiz.questions.count}"
    end
  end
end
