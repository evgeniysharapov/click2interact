class Report < ActiveRecord::Base
  attr_accessible :active, :description, :title, :report_submissions, :child_entry
  has_many :report_sections, inverse_of: :report
  has_many :report_submissions, inverse_of: :report
  has_many :restrictions, :as => :restrictable, class_name: 'ReportRestriction'
  belongs_to :child_entry, class_name: 'ReportEntry'
  has_many :assignments, as: :assignable
end
