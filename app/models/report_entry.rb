class ReportEntry < ActiveRecord::Base
  attr_accessible :content, :entry_type, :display_order
  belongs_to :report_section, inverse_of: :report_entries
  has_many :report_entry_choices, inverse_of: :report_entry
  has_many :entry_values, class_name: 'ReportSubmissionEntry', inverse_of: :report_entry

  validates :entry_type, inclusion: { in: %w(date datetime time integer float text string choice user group) }

  has_many :restrictions, :as => :restrictable, class_name: 'ReportRestriction'

  def role_restrictions
    restrictions.select {|r| r.limitation.is_a? Role}
  end

  def group_restrictions
    restrictions.select {|r| r.limitation.is_a? Group}
  end

  # This works only for the entry of type 'user'
  def user_value_options
    role_names = (role_restrictions.map(&:limitation)).map(&:name)
    group_names = (group_restrictions.map(&:limitation)).map(&:name)
    conds = {}
    if role_names and role_names.any?
      conds[:roles] = { name: role_names }
    end
    if group_names and group_names.any?
      conds[:groups] = { name: role_names }
    end
    # TODO: make sure that only appropriate users are showing up in a list.
    #User.joins(:roles, :groups).where(conds).group('users.id')
    User.joins(:roles).where({roles: {name: :child} }).group('users.id')
  end

  # This works only for the entry of type 'user'
  def group_value_options
    # TODO: add code returning groups that this entry was restricted to
  end

end
