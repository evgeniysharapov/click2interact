class ReportEntryChoice < ActiveRecord::Base
  attr_accessible :content, :display_order
  belongs_to :report_entry, inverse_of: :report_entry_choices
end
