class ReportRestriction < ActiveRecord::Base
  attr_accessible :restrictable, :limitation

  # what is restricted (section, report or report_entry)
  belongs_to :restrictable, polymorphic: true
  # limit could be either a role or a group
  belongs_to :limitation, polymorphic: true
end
