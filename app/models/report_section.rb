class ReportSection < ActiveRecord::Base
  attr_accessible :description, :title, :display_order
  belongs_to :report, inverse_of: :report_sections
  has_many :report_entries, inverse_of: :report_section
  has_many :restrictions, :as => :restrictable, class_name: 'ReportRestriction'

end
