class ReportSubmission < ActiveRecord::Base

  belongs_to :report, inverse_of: :report_submissions
  belongs_to :user
  belongs_to :child, class_name: "User"

  has_many :report_submission_entries, dependent: :destroy, inverse_of: :report_submission, include: :report_entry

  accepts_nested_attributes_for :report_submission_entries, :allow_destroy => true
  attr_accessible :report, :user, :child, :report_submission_entries, :created_at, :updated_at, :report_submission_entries_attributes

  # virtual attribute for keeping track of the sectioning of the report
  attr_accessor :sections
  attr_accessible :sections

  # returns only those submitted by given user
  scope :submitted_by, lambda {|user| where("user_id = ?",user) }

  # returns submissions filed on the day n days ago
  scope :submitted_n_days_ago, lambda {|n| where("created_at >= :start_date and created_at < :end_date", {start_date: (n + 1).days.ago, end_date: n.days.ago })}

  # returns submissions that have child set
  scope :for_children, where("report_submissions.child_id is not NULL")

  # report submission for a children of a given user
  scope :of_parent, lambda { |u| joins("inner join parentings on report_submissions.child_id = parentings.child_id").where("parentings.parent_id = ?", u ) }

  validates_presence_of :user
  validates_presence_of :report
  validates_with SubmissionEntriesValidator

  before_validation :organize_submission_entries

  # This method should be called if we need to organize submission entries for the report
  def organize_submission_entries
    unless self.report.nil?
      self.sections = [] # rehash sections within this report_submission

      # building according to the report
      if self.report_submission_entries.empty?
        self.report.report_sections.map(&:report_entries).flatten.each do |entry|
          sub_entry = self.report_submission_entries.build(report_entry: entry)
          sub_entry.report_submission = self
        end
      end
      tmp = {}
      self.report_submission_entries.each do |sub_entry|
        # make sure all references are set
        sub_entry.report_submission = self
        if sub_entry.report_entry.nil? and sub_entry.report_entry_id
          # It's either bug in Rails or some weird behaviour:
          # forign key is set, but association returns nil
          # let's find that report entry
          sub_entry.report_entry = self.report.report_sections.map(&:report_entries).flatten.find {|e| e.id == sub_entry.report_entry_id}
        end
        tmp[sub_entry.report_entry.report_section] ||= []
        tmp[sub_entry.report_entry.report_section] << sub_entry
      end
      tmp.each_pair do |s, e|
        sec = ReportSubmissionSection.new
        sec.section = s
        sec.entries = e
        self.sections << sec
      end
    end
    self
  end

end
