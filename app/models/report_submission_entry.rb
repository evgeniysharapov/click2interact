class ReportSubmissionEntry < ActiveRecord::Base
  belongs_to :report_submission, inverse_of: :report_submission_entries, include: :report
  belongs_to :report_entry, inverse_of: :entry_values
  belongs_to :choice_value, class_name: "ReportEntryChoice", foreign_key: "report_entry_choice_id"
  belongs_to :user_value, class_name: "User", foreign_key: "user_id"
  belongs_to :group_value, class_name: "Group", foreign_key: "group_id"

  attr_accessible  :report_submission, :report_entry, :report_submission_id, :report_entry_id, :created_at, :updated_at, :report_entry_choice_id, :user_id, :group_id
  attr_accessible :date_value,
                  :time_value,
                  :datetime_value,
                  :integer_value,
                  :float_value,
                  :text_value,
                  :string_value,
                  :user_value,
                  :group_value

# TODO: check if breaks
#  validates_presence_of :report_entry
  validates_presence_of :report_submission

  # validates_presence_of :string_value, if: lambda { self.report_entry.entry_type == "string"}
  # validates_presence_of :date_value, if: lambda { self.report_entry.entry_type == "date"}
  # validates_presence_of :time_value, if: lambda { self.report_entry.entry_type == "time"}
  # validates_presence_of :integer_value, if: lambda { self.report_entry.entry_type == "integer"}
  # validates_presence_of :float_value, if: lambda { self.report_entry.entry_type == "float"}
  # validates_presence_of :text_value, if: lambda { self.report_entry.entry_type == "text"}
  # validates_presence_of :user_value, if: lambda { self.report_entry.entry_type == "user"}
  # validates_presence_of :group_value, if: lambda { self.report_entry.entry_type == "group"}

  after_save :set_child_entry_value

  def time_value
    read_attribute(:datetime_value).strftime( time_format ) unless read_attribute(:datetime_value).blank?
  end

  def time_value=(val)
    unless val.empty?
      if  Time.zone.parse("#{Date.today.to_s} #{val}").nil?
        raise Click::Errors::ParsingError, "Could not parse '#{val}' as a time value."
      end
      self.datetime_value = Time.zone.parse("#{Date.today.to_s} #{val}").to_datetime
    end
  end

  def date_value
    read_attribute(:datetime_value).strftime( date_format ) unless read_attribute(:datetime_value).blank?
  end

  def date_value=(val)
    unless val.empty?
      if Time.zone.parse(val).nil?
        raise Click::Errors::ParsingError, "Could not parse '#{val}' as a date value."
      end
      self.datetime_value = Time.zone.parse(val).to_datetime
    end
  end

  def time_format
    '%H:%M'
  end

  def date_format
    '%Y-%m-%d'
  end

  def datetime_format
    '%Y-%m-%d %H:%M'
  end

  def to_s
    if self.report_entry.entry_type == "choice" and self.choice_value
      return self.choice_value.content
    elsif self.report_entry.entry_type == "user" and self.user_value
      return self.user_value.name # returns user's name
    elsif self.report_entry.entry_type == "group" and self.group_value
      return self.group_value.name # returns group's name
    elsif %w(date datetime time).include? self.report_entry.entry_type
      s = self.report_entry.entry_type.to_s
      return eval("(self.#{self.report_entry.entry_type.to_s}_value).to_s")
    else
      return "#{(self.string_value || self.text_value || self.integer_value || self.float_value || nil).to_s}"
    end
  end

  protected
  # If the corresponding entry is a child entry for a report (see Report#child_entry),
  # then we will set ReportFile#child_id to a value of this ReportFileEntry
  def set_child_entry_value
    #puts self.attributes
    #debugger
    if self.report_entry == self.report_submission.report.child_entry
      # it is a child entry
      # do an actual update
      self.report_submission.update_attributes(child: self.user_value)
    end
  end

end
