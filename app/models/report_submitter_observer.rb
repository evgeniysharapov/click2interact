# This class observes ReportSubmissions and if ReportSubmission
# has a child in it, then it sends AnnouncementMessage to their
# parent
class ReportSubmitterObserver < ActiveRecord::Observer
  observe :report_submission

  # after updating a report_submission we check if it has child and
  # then inform parents about submitted report
  def after_update(report_submission)
    unless report_submission.child.nil?
      am = AnnouncementMessage.new(author: report_submission.user)
      # TODO: take out content into a separate translation file or something
      am.content = "#{report_submission.user.name} submitted report '#{report_submission.report.title}' for #{report_submission.child.name}"
      # TODO: make more universal url creator
      am.link = Rails.application.routes.url_helpers.parent_report_report_submission_path(report_submission.report, report_submission)
      User.parents_of(report_submission.child).each do |usr|
          am.announce_to(usr)
      end
      am.save!
    end
  end

end
