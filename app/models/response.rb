# == Schema Information
#
# Table name: responses
#
#  id              :integer          not null, primary key
#  response_set_id :integer
#  reply_id        :integer
#  article_id      :integer
#  integer_value   :integer
#  float_value     :float
#  string_value    :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Response < ActiveRecord::Base
  attr_accessible :article, :integer_value, :float_value,:string_value

  # this is because of the fill form
  attr_accessible :article_id, :response_set_id, :reply_id

  belongs_to :article
  belongs_to :reply
  belongs_to :response_set

  validates :article_id, presence: true
  validates :reply_id, presence: true, if: Proc.new { |r| r.article.response_type == 'choice' }

end
