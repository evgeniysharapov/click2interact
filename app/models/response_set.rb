# == Schema Information
#
# Table name: response_sets
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  paper_form_id :integer
#  completed_on  :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  assignment_id :integer
#

class ResponseSet < ActiveRecord::Base
  attr_accessible :user, :paper_form
  belongs_to :user
  belongs_to :paper_form
  belongs_to :assignment, inverse_of: :response_sets

  has_many :responses, dependent: :destroy

  accepts_nested_attributes_for :responses, :allow_destroy => true
  attr_accessible :responses_attributes

  validates :paper_form_id, :assignment_id, presence: true
  validates_associated :responses

  attr_accessible :completed_on

  def complete!
    assignment.complete!
    self.update_attributes completed_on: Time.now
  end

end
