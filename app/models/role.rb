# == Schema Information
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  resource_id   :integer
#  resource_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  MAIN_ROLES = [:admin, :teacher, :parent, :child]

  def self.main_roles
    where(name: MAIN_ROLES)
  end

  def self.other_roles
    where('name not in(?)', MAIN_ROLES)
  end
  
  scopify
end
