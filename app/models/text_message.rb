# == Schema Information
#
# Table name: text_messages
#
#  id         :integer          not null, primary key
#  content    :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TextMessage < ActiveRecord::Base
  attr_accessible :content , :sender

  has_many :text_message_users, :dependent => :destroy
  has_many :recipients, :through => :text_message_users, :source => :user
  belongs_to :sender, :class_name => 'User', :inverse_of => :sms_to_send, foreign_key: 'user_id'

  def self.create_for (*attributes, &blk)
    opts = attributes.extract_options!
    opts[:recipient_type] ||= 'User'
    records = []
    if opts[:recipient_type] == 'User'  # create single instance
      opts[:recipient] = User.find opts[:recipient]
      record = self.new opts.except(:recipient_type).except(:recipient)
      record.recipients << opts[:recipient]
      # if there's any action execute it first
      if block_given?
        blk.call(record)
      end
      record.save
      records << record
      # we add a method returning recipient type to the result
      def records.recipient_type; 'User'; end

    elsif opts[:recipient_type] == 'Group' # create bunch of models
      # find a group
      group = Group.find opts[:recipient]
      record = self.new opts.except(:recipient_type).except(:recipient)
      record.recipients << group.users
      # if there's any action execute it first
      if block_given?
        blk.call(record)
      end
      record.save
      records << record
      def records.recipient_type; 'Group'; end
    end
    records
  end

  def self.from(u)
    create {|m| m.sender = u }
  end

  def send_text
    # check that we have a sender and recipient
    # after this one can change content.
    errors = []
    recipients.each do |recipient|
      if recipient.profile.nil?
        errors << "No profile for user #{recipient.name} has been established. Cannot send him a text message."
      elsif recipient.profile.carrier.nil?
	errors << "Couldn't find an appropriate carrier for phone number #{recipient.profile.phone}"
      else
        phone = recipient.profile.phone
        carrier = Profile::CARRIERS_NAMES[recipient.profile.carrier.to_sym]
        m = TextMessageMailer.text_message_email(sender.name, phone, carrier, content)
        m.deliver!
      end
    end
    # raise errors to the calling code. Text messages have been sent
    # to who we could send them
    save
    return errors
  end
end
