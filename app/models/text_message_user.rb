# == Schema Information
#
# Table name: text_message_users
#
#  id              :integer          not null, primary key
#  text_message_id :integer
#  user_id         :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class TextMessageUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :text_message
end
