class User < ActiveRecord::Base
  rolify
  attr_accessible :role_ids
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :profile_attributes
  attr_accessible :pin

  # This will take care of approval
  attr_accessible :approved
  after_update :send_on_approval_confirmation_instructions
  before_update :set_pin

  acts_as_messageable

  has_one :profile, :dependent => :destroy, :inverse_of => :user
  accepts_nested_attributes_for :profile, :reject_if => :all_blank

 # Child <-> Parent relationship
  attr_accessible :child_ids
  has_many :parent_child_relationships,
           :class_name => "Parenting",
           :foreign_key => :child_id,
           :dependent => :destroy
  has_many :parents,
           :through => :parent_child_relationships,
           :source => :parent,
           :uniq => true
  has_many :child_parent_relationships,
           :class_name => "Parenting",
           :foreign_key => :parent_id,
           :dependent => :destroy
  has_many :children,
           :through => :child_parent_relationships,
           :source => :child,
           :uniq => true

  scope :all_children, joins(:roles).where('roles.name' => 'child')
  scope :all_teachers, joins(:roles).where('roles.name' => 'teacher')
  scope :all_parents, joins(:roles).where('roles.name' => 'parent')
  scope :all_admins, joins(:roles).where('roles.name' => 'admin')

  scope :children_of, lambda{ |u| joins(:parents).where(parentings: {parent_id: u}) }
  scope :parents_of, lambda{ |u| joins(:children).where(parentings: {child_id: u}) }

 ### Text messaging part of the user
  has_many :text_message_users
  has_many :sms_to_receive, :through => :text_message_users, :source => :text_message
  has_many :sms_to_send, :class_name => 'TextMessage', :inverse_of => :sender

  # returns those users that have sent the message
  def self.recipients_of(msg)
    joins(:sms_to_receive).where(:text_messages => {:id => msg})
  end

  # groupping
  has_and_belongs_to_many :groups

  # changes groups user belongs to to a given list of group names
  def change_groups_to(groups)
    self.groups.clear
    groups.each do |g|
      self.groups << Group.where(name: g).first
    end
  end
  # changes roles of the user to a given list of roles names
  def change_roles_to(roles)
    self.roles.clear
    roles.each do |r|
      self.add_role r
    end
  end


  # Returns the list of possible recipients(users or groups) of the
  # Sendable object that coule be send a message, announcement
  # and alike by current_user
  def find_recipients(recipient_type)
    # TODO: introduce limitations based on roles or something
    # TODO: somehow bind it with Sendable and whatever class includes
    # it
    if recipient_type == "Group"
      Group.all
    elsif recipient_type == "User"
      User.all
    else
      []
    end
  end

  # Returns true if there were announcements for the user and false otherwise
  def has_past_announcements?
    not Announcement.where(recipient_id: self, showable: false).empty?
  end

  # overriding Devise methods for signing up using only email
  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |user|
        csv << user.attributes.values_at(*column_names)
      end
    end
  end

protected

  # Override callback from the Devise library
  # We don't send an email out until user gets approved
  def send_on_create_confirmation_instructions
    #send_devise_notification(:confirmation_instructions)
    logger.debug "User was just created"
  end

  # This will be triggerred when approved flag is set to true
  # unless PIN is set, then we don't send a confirmation 
  # instructions
  def send_on_approval_confirmation_instructions
    send_devise_notification(:confirmation_instructions) if self.pin.nil?
  end

  # Setting PIN automatically approves the user
  def set_pin
    unless self.pin.nil?
      self.approved = true
    end
  end

end
