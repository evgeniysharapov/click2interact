FormAssignmentsPresenter = Struct.new(:form_assignments) do
  # returns list of pending form assignments
  def pending
    form_assignments.find_all do |fa|
      fa.completed_at.nil?
    end
  end
  # returns list of completed form assignments
  def completed
    form_assignments.find_all do |fa|
      not fa.completed_at.nil?
    end
  end
end
