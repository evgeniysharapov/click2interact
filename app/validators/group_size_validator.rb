class GroupSizeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if record.group_id
      unless Group.find(record.group_id).users.count == value
        record.errors.add attribute, (options[:message] || "should have as many as users in the group")
      end
    end
  end
end
