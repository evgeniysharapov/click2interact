class SubmissionEntriesValidator < ActiveModel::Validator
  # Validates report submission having enough report submission entries
  def validate(record)
    unless record.report.nil?
      report_entries_number = record.report.report_sections.map(&:report_entries).flatten.size
      submission_entries_number = record.report_submission_entries.size
      unless  report_entries_number == submission_entries_number
        record.errors[:report_submission_entries] << "number of entries in report (#{report_entries_number}) and number of entries in report submission (#{submission_entries_number}) should be the same."
      end
      rep_entr_ids = record.report.report_sections.map(&:report_entries).flatten.map(&:id)
      subm_rep_entr_ids = record.report_submission_entries.map(&:report_entry_id)

      unless (rep_entr_ids - subm_rep_entr_ids).empty? and (subm_rep_entr_ids - rep_entr_ids).empty?
        record.errors[:report_submission_entries] << "submission entries' report_entry should refer to the report entries in the report"
      end
    end
  end
end
