set :rvm_type, :system
require 'rvm/capistrano'
require 'bundler/capistrano'

set :application, "click2interact"
set :repository,  "git@bitbucket.org:evgeniysharapov/click2interact.git"
set :scm, :git
set :branch, 'master'
set :host, "click2interact.com"

role :web, "#{host}"                          # Your HTTP server, Apache/etc
role :app, "#{host}"                          # This may be the same as your `Web` server
role :db,  "#{host}", :primary => true  # This is where Rails migrations will run

default_run_options[:pty] = false
ssh_options[:forward_agent] = true
set :use_sudo, false
set :user, "app_user"

set :rails_env, 'production'

set :deploy_via, :copy
set :copy_strategy, :export
set :deploy_to, "/var/www/#{host}"

set :bundle_without, [:darwin, :development, :test]

# if you want to clean up old releases on each deploy uncomment this:
#after "deploy:restart", "deploy:cleanup"

namespace :deploy do

  desc "Zero-downtime restart of Unicorn"
  task :restart, roles: :app, :except => { :no_release => true } do
    run "kill -s USR2 `cat /tmp/unicorn.#{application}.pid`"
  end

  desc "Start Unicorn"
  task :start, roles: :app, :except => { :no_release => true } do
    run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
  end

  desc "Stop Unicorn"
  task :stop, roles: :app, :except => { :no_release => true } do
    run "kill -s QUIT `cat /tmp/unicorn.#{application}.pid`"
  end

  desc "Copy the database.yml file into the latest release"
  task :copy_in_database_yml do
    run "cp #{shared_path}/config/database.yml #{latest_release}/config/"
  end

  namespace :db do

    desc "Create Production Database"
    task :create do
      puts "\n\n=== Creating the Production Database! ===\n\n"
      run "cd #{current_path}; bundle exec rake db:create RAILS_ENV=production"
    end

    desc "Migrate Production Database"
    task :migrate do
      puts "\n\n=== Migrating the Production Database! ===\n\n"
      run "cd #{current_path}; bundle exec rake db:migrate RAILS_ENV=production"
    end

    desc "Resets the Production Database"
    task :migrate_reset do
      puts "\n\n=== Resetting the Production Database! ===\n\n"
      run "cd #{current_path}; bundle exec rake db:migrate:reset RAILS_ENV=production"
    end

    desc "Destroys Production Database"
    task :drop do
      puts "\n\n=== Destroying the Production Database! ===\n\n"
      run "cd #{current_path}; bundle exec rake db:drop RAILS_ENV=production"
    end

    desc "Populates the Production Database"
    task :seed do
      puts "\n\n=== Populating the Production Database! ===\n\n"
      run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=production"
    end
  end

end

before "deploy:assets:precompile", "deploy:copy_in_database_yml"

after "deploy", "deploy:migrate"
