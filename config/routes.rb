Click2Interact::Application.routes.draw do

  resources :announcement_messages, path: 'announcements' do
    get :unshow
    # shows list of past announcements for the current user
    collection do
      get :past
    end
    member do
      get :recipients
      post :resend
    end
  end

  resources :groups

  resources :paper_forms, path: 'forms' do
    resources :assignments do
      member do
        get :take
        post :complete
        get :completed
      end
    end
    resources :response_sets

    get :possible_assignees

  end

  resources :quizzes do
    resources :assignments do
      member do
        get :take
        post :complete
        get :completed
      end
    end
  end

  authenticated :user do
    root :to => 'home#index'
  end

  root :to => "pages#index"
  match '/terms' => "pages#terms"
  match '/about' => "pages#about"
  match '/faq' => "pages#faq"
  match '/changelog' => "pages#changelog"

  # authentification and registration
  devise_for :users, controllers: { registrations: "registrations", confirmations: "confirmations" } do
    # sign-up with PIN
    get "users/sign_up_with_pin", to: "registrations#new_pin"
  end
  devise_scope :user do
    put "/confirm" => "confirmations#confirm"
  end

  resources :users, :only => [:show, :index, :edit] do
    resources :assignments do
      collection do
        get 'for'
        get 'by'
      end
    end
    get 'recipient_list'
    put 'change'

    get 'new_child'
    post 'create_child'

    resources :announcement_messages, path: :announcements, only: [:resend]  do
      post :resend
    end

    resources :announcement_messages, path: :announcements, only: [:index]
    
  end

  resources :conversations, only: [:index, :show, :new, :create] do
    member do
      post :reply
      post :trash
      post :untrash
    end
  end

  resources :text_messages, :path => 'sms', :only => [:index, :new, :create, :destroy]

  namespace :admin do
    resources :reports
    resources :users, :only => [:show, :edit, :index, :new, :create, :destroy] do
      put 'change'
      get 'new_child'
      post 'create_child'
      member do
        post 'approve'
        post 'disapprove'
        post 'notify'
      end
    end
    resources :user_imports

    root to: "users#index"
  end

  scope '/admin' do
    resources :reports do
      resources :assignments
    end
  end

  namespace :teacher do
    resources :reports, only: [:index], controller: 'report_submissions' do
      resources :report_submissions, only: [:new, :show, :create, :destroy] , path: 'submissions'
      get "submissions", controller: 'report_submissions'
    end
  end

  namespace :parent do
    resources :reports, only: [:index], controller: 'report_submissions' do
      resources :report_submissions, only: [:show] , path: 'submissions'
      get "submissions", controller: 'report_submissions'
    end
  end

end
#== Route Map
# Generated on 15 Oct 2012 21:50
#
#                                 POST   /groups(.:format)                                         groups#create
#                       new_group GET    /groups/new(.:format)                                     groups#new
#                      edit_group GET    /groups/:id/edit(.:format)                                groups#edit
#                           group GET    /groups/:id(.:format)                                     groups#show
#                                 PUT    /groups/:id(.:format)                                     groups#update
#                                 DELETE /groups/:id(.:format)                                     groups#destroy
#      take_paper_form_assignment GET    /forms/:paper_form_id/assignments/:id/take(.:format)      assignments#take
#  complete_paper_form_assignment POST   /forms/:paper_form_id/assignments/:id/complete(.:format)  assignments#complete
# completed_paper_form_assignment GET    /forms/:paper_form_id/assignments/:id/completed(.:format) assignments#completed
#          paper_form_assignments GET    /forms/:paper_form_id/assignments(.:format)               assignments#index
#                                 POST   /forms/:paper_form_id/assignments(.:format)               assignments#create
#       new_paper_form_assignment GET    /forms/:paper_form_id/assignments/new(.:format)           assignments#new
#      edit_paper_form_assignment GET    /forms/:paper_form_id/assignments/:id/edit(.:format)      assignments#edit
#           paper_form_assignment GET    /forms/:paper_form_id/assignments/:id(.:format)           assignments#show
#                                 PUT    /forms/:paper_form_id/assignments/:id(.:format)           assignments#update
#                                 DELETE /forms/:paper_form_id/assignments/:id(.:format)           assignments#destroy
#        paper_form_response_sets GET    /forms/:paper_form_id/response_sets(.:format)             response_sets#index
#                                 POST   /forms/:paper_form_id/response_sets(.:format)             response_sets#create
#     new_paper_form_response_set GET    /forms/:paper_form_id/response_sets/new(.:format)         response_sets#new
#    edit_paper_form_response_set GET    /forms/:paper_form_id/response_sets/:id/edit(.:format)    response_sets#edit
#         paper_form_response_set GET    /forms/:paper_form_id/response_sets/:id(.:format)         response_sets#show
#                                 PUT    /forms/:paper_form_id/response_sets/:id(.:format)         response_sets#update
#                                 DELETE /forms/:paper_form_id/response_sets/:id(.:format)         response_sets#destroy
#   paper_form_possible_assignees GET    /forms/:paper_form_id/possible_assignees(.:format)        paper_forms#possible_assignees
#                     paper_forms GET    /forms(.:format)                                          paper_forms#index
#                                 POST   /forms(.:format)                                          paper_forms#create
#                  new_paper_form GET    /forms/new(.:format)                                      paper_forms#new
#                 edit_paper_form GET    /forms/:id/edit(.:format)                                 paper_forms#edit
#                      paper_form GET    /forms/:id(.:format)                                      paper_forms#show
#                                 PUT    /forms/:id(.:format)                                      paper_forms#update
#                                 DELETE /forms/:id(.:format)                                      paper_forms#destroy
#                            root        /                                                         home#index
#                            root        /                                                         pages#index
#                new_user_session GET    /users/sign_in(.:format)                                  devise/sessions#new
#                    user_session POST   /users/sign_in(.:format)                                  devise/sessions#create
#            destroy_user_session DELETE /users/sign_out(.:format)                                 devise/sessions#destroy
#                   user_password POST   /users/password(.:format)                                 devise/passwords#create
#               new_user_password GET    /users/password/new(.:format)                             devise/passwords#new
#              edit_user_password GET    /users/password/edit(.:format)                            devise/passwords#edit
#                                 PUT    /users/password(.:format)                                 devise/passwords#update
#        cancel_user_registration GET    /users/cancel(.:format)                                   devise/registrations#cancel
#               user_registration POST   /users(.:format)                                          devise/registrations#create
#           new_user_registration GET    /users/sign_up(.:format)                                  devise/registrations#new
#          edit_user_registration GET    /users/edit(.:format)                                     devise/registrations#edit
#                                 PUT    /users(.:format)                                          devise/registrations#update
#                                 DELETE /users(.:format)                                          devise/registrations#destroy
#            for_user_assignments GET    /users/:user_id/assignments/for(.:format)                 assignments#for
#             by_user_assignments GET    /users/:user_id/assignments/by(.:format)                  assignments#by
#                user_assignments GET    /users/:user_id/assignments(.:format)                     assignments#index
#                                 POST   /users/:user_id/assignments(.:format)                     assignments#create
#             new_user_assignment GET    /users/:user_id/assignments/new(.:format)                 assignments#new
#            edit_user_assignment GET    /users/:user_id/assignments/:id/edit(.:format)            assignments#edit
#                 user_assignment GET    /users/:user_id/assignments/:id(.:format)                 assignments#show
#                                 PUT    /users/:user_id/assignments/:id(.:format)                 assignments#update
#                                 DELETE /users/:user_id/assignments/:id(.:format)                 assignments#destroy
#                           users GET    /users(.:format)                                          users#index
#                            user GET    /users/:id(.:format)                                      users#show
#                         quizzes GET    /quizzes(.:format)                                        quizzes#index
#                                 POST   /quizzes(.:format)                                        quizzes#create
#                        new_quiz GET    /quizzes/new(.:format)                                    quizzes#new
#                       edit_quiz GET    /quizzes/:id/edit(.:format)                               quizzes#edit
#                            quiz GET    /quizzes/:id(.:format)                                    quizzes#show
#                                 PUT    /quizzes/:id(.:format)                                    quizzes#update
#                                 DELETE /quizzes/:id(.:format)                                    quizzes#destroy
#              reply_conversation POST   /conversations/:id/reply(.:format)                        conversations#reply
#              trash_conversation POST   /conversations/:id/trash(.:format)                        conversations#trash
#            untrash_conversation POST   /conversations/:id/untrash(.:format)                      conversations#untrash
#                   conversations GET    /conversations(.:format)                                  conversations#index
#                                 POST   /conversations(.:format)                                  conversations#create
#                new_conversation GET    /conversations/new(.:format)                              conversations#new
#                    conversation GET    /conversations/:id(.:format)                              conversations#show
#                   text_messages GET    /sms(.:format)                                            text_messages#index
#                                 POST   /sms(.:format)                                            text_messages#create
#                new_text_message GET    /sms/new(.:format)                                        text_messages#new
#                    text_message DELETE /sms/:id(.:format)                                        text_messages#destroy
