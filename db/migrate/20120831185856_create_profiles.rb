class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :phone
      t.string :carrier

      t.timestamps
    end
  end
end
