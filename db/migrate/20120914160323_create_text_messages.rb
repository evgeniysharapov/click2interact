class CreateTextMessages < ActiveRecord::Migration
  def change
    create_table :text_messages do |t|
      t.string :content
      t.integer :user_id
      t.timestamps
    end

    create_table :text_message_users do |t|
      t.integer :text_message_id
      t.integer :user_id
      t.timestamps
    end

    add_column :users, :text_message_id, :integer

  end

end
