class CreatePaperForms < ActiveRecord::Migration
  def change
    create_table :paper_forms do |t|
      t.string :title
      t.integer :author_id
      t.timestamps
    end
  end
end
