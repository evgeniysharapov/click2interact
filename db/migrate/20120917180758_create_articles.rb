class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :paper_form_id
      t.text :content
      t.string :response_type
      t.timestamps
    end
  end
end
