class CreateResponseSets < ActiveRecord::Migration
  def change
    create_table :response_sets do |t|
      t.integer :user_id
      t.integer :paper_form_id
      t.datetime :completed_on
      t.timestamps
    end
  end
end
