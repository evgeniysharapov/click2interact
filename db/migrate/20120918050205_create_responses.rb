class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.integer :response_set_id
      t.integer :reply_id
      t.integer :article_id
      t.integer :integer_value
      t.float   :float_value
      t.string  :string_value
      t.timestamps
    end
  end
end
