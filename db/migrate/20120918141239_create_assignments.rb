class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :from_user_id
      t.integer :to_user_id
      t.date :due_date
      t.datetime :completed_at
      t.references  :assignable, polymorphic: true

      t.timestamps
    end
  end
end
