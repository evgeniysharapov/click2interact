class AddAssignmentIdToResponseSets < ActiveRecord::Migration
  def change
    add_column :response_sets, :assignment_id, :integer
  end
end
