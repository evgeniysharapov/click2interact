class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.timestamps
    end

    create_table :groups_users, id: false do |t|
      t.integer :group_id, null: false
      t.integer :user_id, null: false
    end

    add_index :groups_users, [:group_id, :user_id], unique: true, name: :groups_users_ids

  end

end
