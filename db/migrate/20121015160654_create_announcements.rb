class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.text :content
      t.integer :user_id
      t.integer :author_id
      t.boolean :showable

      t.timestamps
    end
  end
end
