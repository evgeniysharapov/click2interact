class AddDefaultValueToAnnouncementShowable < ActiveRecord::Migration
  def up
    change_column :announcements, :showable, :boolean, default: true
  end

  def down
    change_column :announcements, :showable, :boolean, default: nil
  end
end
