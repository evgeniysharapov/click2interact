class ChangeColumnNamesForSendable < ActiveRecord::Migration
  def change
    rename_column :assignments, :from_user_id, :sender_id
    rename_column :assignments, :to_user_id, :recipient_id
    rename_column :announcements, :user_id, :recipient_id
    rename_column :announcements, :author_id, :sender_id
  end
end
