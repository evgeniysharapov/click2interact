class CreateAnnouncementMessages < ActiveRecord::Migration
  def change
    create_table :announcement_messages do |t|
      t.text :content
      t.integer :author_id
      t.integer :group_id

      t.timestamps
    end
  end
end
