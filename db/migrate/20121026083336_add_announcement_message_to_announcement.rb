class AddAnnouncementMessageToAnnouncement < ActiveRecord::Migration
  def change
    change_table :announcements do |t|
      t.remove :content
      t.integer :message_id
    end
  end
end
