class AddAttachmentToAnnouncementMessages < ActiveRecord::Migration
  def change
    add_attachment :announcement_messages, :attachment
  end
end
