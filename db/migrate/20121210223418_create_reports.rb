class CreateReports < ActiveRecord::Migration
  def change
    # this is a template for a report
    create_table :reports do |t|
      t.string :title
      t.text :description
      t.boolean :active
    end

    create_table :report_sections do |t|
      t.references :report
      t.string :title
      t.text :description
      t.integer :display_order
    end

    create_table :report_entries do |t|
      t.references :report_section
      t.text :content
      t.string :entry_type
      t.integer :display_order
    end

    create_table :report_entry_choices do |t|
      t.references :report_entry
      t.text :content
      t.integer :display_order
    end

    # this is an actual data file for a report
    create_table :report_files do |t|
      t.references :report
      t.references :user
      t.integer :child_id
      t.timestamps
    end

    create_table :report_file_entries do |t|
      t.references :report_file
      t.references :report_entry
      # different value types
      t.datetime :datetime_value # holds date/datetime/time values
      t.integer :integer_value
      t.float :float_value
      t.text :text_value
      t.string :string_value
      t.integer :report_entry_choice_id
      t.timestamps
    end

  end
end
