class CreateParentings < ActiveRecord::Migration
  def change
    create_table :parentings do |t|
      t.references :parent
      t.references :child

      t.timestamps
    end
    add_index :parentings, :parent_id
    add_index :parentings, :child_id
  end
end
