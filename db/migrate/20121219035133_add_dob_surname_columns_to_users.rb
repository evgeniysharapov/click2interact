class AddDobSurnameColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dob, :date
    add_column :users, :surname, :string
  end
end
