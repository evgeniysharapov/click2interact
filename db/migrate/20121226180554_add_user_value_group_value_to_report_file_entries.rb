class AddUserValueGroupValueToReportFileEntries < ActiveRecord::Migration
  def change
    add_column :report_file_entries, :user_id, :integer
    add_column :report_file_entries, :group_id, :integer
  end
end
