class CreateReportRestrictions < ActiveRecord::Migration
  def change
    create_table :report_restrictions do |t|
      t.references :restrictable, polymorphic: true
      t.references :limitation, polymorphic: true
      t.timestamps
    end
  end
end
