class AddChildEntryColumnToReports < ActiveRecord::Migration
  def change
    add_column :reports, :child_entry_id, :integer
  end
end
