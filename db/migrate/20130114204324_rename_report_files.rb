class RenameReportFiles < ActiveRecord::Migration
  def change
    rename_table :report_files, :report_submissions
    rename_table :report_file_entries, :report_submission_entries
    rename_column :report_submission_entries, :report_file_id, :report_submission_id
  end
end
