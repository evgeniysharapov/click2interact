class AddSystemLinkToAnnouncementMessages < ActiveRecord::Migration
  def change
    add_column :announcement_messages, :link, :string
  end
end
