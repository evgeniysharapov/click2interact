class CreateAnswerValues < ActiveRecord::Migration
  def change
    create_table :quiz_takes do |t|
      t.references :user
      t.references :assignment
      t.references :quiz
      t.datetime :started_at
      t.datetime :completed_at
      t.boolean :completed
    end

    add_index :quiz_takes, :user_id
    add_index :quiz_takes, :assignment_id
    add_index :quiz_takes, :quiz_id

    create_table :answer_values do |t|
      t.references :question
      t.references :answer
      t.references :quiz_take
      t.timestamps
    end

    add_index :answer_values, :question_id
    add_index :answer_values, :answer_id
    add_index :answer_values, :quiz_take_id

  end
end
