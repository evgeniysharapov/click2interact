class AddOwnerToGroups < ActiveRecord::Migration
  def change
    change_table :groups do |t|
      t.references :owner
    end
  end
end
