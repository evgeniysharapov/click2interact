class AddApprovalToUser < ActiveRecord::Migration
  def up
    add_column :users, :approved, :boolean, default: false
    # assume all users have been approved already
    ActiveRecord::Base.connection.execute('UPDATE `users` SET `approved`=1 WHERE 1')
  end

  def down
    remove_column :users, :approved, :boolean
  end
end
