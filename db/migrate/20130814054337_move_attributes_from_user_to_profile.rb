class MoveAttributesFromUserToProfile < ActiveRecord::Migration
  def up
    add_column :profiles, :bio, :text
    add_column :profiles, :dob, :date
    add_column :profiles, :surname, :string
    execute "UPDATE profiles SET bio = (select u.bio FROM users u, profiles p WHERE u.id = p.user_id), dob = (select u.dob FROM users u, profiles p WHERE u.id = p.user_id), surname = (select u.surname FROM users u, profiles p WHERE u.id = p.user_id)"
    remove_column :users, :bio
    remove_column :users, :dob
    remove_column :users, :surname
  end

  def down
    add_column :users, :bio, :text
    add_column :users, :dob, :date
    add_column :users, :surname, :string
    execute "UPDATE users SET bio = (select p.bio FROM users u, profiles p WHERE u.id = p.user_id), dob = (select p.dob FROM users u, profiles p WHERE u.id = p.user_id), surname = (select p.surname FROM users u, profiles p WHERE u.id = p.user_id)"
    remove_column :profiles, :bio
    remove_column :profiles, :dob
    remove_column :profiles, :surname
  end
end
