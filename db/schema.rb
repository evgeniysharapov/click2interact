# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130814141534) do

  create_table "announcement_messages", :force => true do |t|
    t.text     "content"
    t.integer  "author_id",               :limit => 11
    t.integer  "group_id",                :limit => 11
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size",    :limit => 11
    t.datetime "attachment_updated_at"
    t.string   "link"
  end

  create_table "announcements", :force => true do |t|
    t.integer  "recipient_id", :limit => 11
    t.integer  "sender_id",    :limit => 11
    t.integer  "showable",     :limit => 1,  :default => 1
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "message_id",   :limit => 11
  end

  create_table "answer_values", :force => true do |t|
    t.integer  "question_id",  :limit => 11
    t.integer  "answer_id",    :limit => 11
    t.integer  "quiz_take_id", :limit => 11
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "answer_values", ["answer_id"], :name => "answer_values_index_answer_values_on_answer_id"
  add_index "answer_values", ["question_id"], :name => "answer_values_index_answer_values_on_question_id"
  add_index "answer_values", ["quiz_take_id"], :name => "answer_values_index_answer_values_on_quiz_take_id"

  create_table "answers", :force => true do |t|
    t.string   "content"
    t.integer  "question_id", :limit => 11
    t.integer  "correct",     :limit => 1
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "articles", :force => true do |t|
    t.integer  "paper_form_id", :limit => 11
    t.text     "content"
    t.string   "response_type"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "assignments", :force => true do |t|
    t.integer  "sender_id",       :limit => 11
    t.integer  "recipient_id",    :limit => 11
    t.date     "due_date"
    t.datetime "completed_at"
    t.integer  "assignable_id",   :limit => 11
    t.string   "assignable_type"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "conversations", :force => true do |t|
    t.string   "subject",    :default => ""
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.integer  "owner_id",   :limit => 11
  end

  create_table "groups_users", :id => false, :force => true do |t|
    t.integer "group_id", :limit => 11, :null => false
    t.integer "user_id",  :limit => 11, :null => false
  end

  add_index "groups_users", ["group_id", "user_id"], :name => "groups_users_groups_users_ids", :unique => true

  create_table "notifications", :force => true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",                            :default => ""
    t.integer  "sender_id",            :limit => 11
    t.string   "sender_type"
    t.integer  "conversation_id",      :limit => 11
    t.integer  "draft",                :limit => 1,  :default => 0
    t.datetime "updated_at",                                         :null => false
    t.datetime "created_at",                                         :null => false
    t.integer  "notified_object_id",   :limit => 11
    t.string   "notified_object_type"
    t.string   "notification_code"
    t.string   "attachment"
  end

  add_index "notifications", ["conversation_id"], :name => "notifications_index_notifications_on_conversation_id"

  create_table "paper_forms", :force => true do |t|
    t.string   "title"
    t.integer  "author_id",  :limit => 11
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "parentings", :force => true do |t|
    t.integer  "parent_id",  :limit => 11
    t.integer  "child_id",   :limit => 11
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "parentings", ["child_id"], :name => "parentings_index_parentings_on_child_id"
  add_index "parentings", ["parent_id"], :name => "parentings_index_parentings_on_parent_id"

  create_table "profiles", :force => true do |t|
    t.integer  "user_id",    :limit => 11
    t.string   "phone"
    t.string   "carrier"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.text     "bio"
    t.date     "dob"
    t.string   "surname"
  end

  create_table "questions", :force => true do |t|
    t.integer  "quiz_id",    :limit => 11
    t.text     "content"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "quiz_takes", :force => true do |t|
    t.integer  "user_id",       :limit => 11
    t.integer  "assignment_id", :limit => 11
    t.integer  "quiz_id",       :limit => 11
    t.datetime "started_at"
    t.datetime "completed_at"
    t.integer  "completed",     :limit => 1
  end

  add_index "quiz_takes", ["assignment_id"], :name => "quiz_takes_index_quiz_takes_on_assignment_id"
  add_index "quiz_takes", ["quiz_id"], :name => "quiz_takes_index_quiz_takes_on_quiz_id"
  add_index "quiz_takes", ["user_id"], :name => "quiz_takes_index_quiz_takes_on_user_id"

  create_table "quizzes", :force => true do |t|
    t.string   "title"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "receipts", :force => true do |t|
    t.integer  "receiver_id",     :limit => 11
    t.string   "receiver_type"
    t.integer  "notification_id", :limit => 11,                :null => false
    t.integer  "is_read",         :limit => 1,  :default => 0
    t.integer  "trashed",         :limit => 1,  :default => 0
    t.integer  "deleted",         :limit => 1,  :default => 0
    t.string   "mailbox_type",    :limit => 25
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
  end

  add_index "receipts", ["notification_id"], :name => "receipts_index_receipts_on_notification_id"

  create_table "replies", :force => true do |t|
    t.integer  "article_id", :limit => 11
    t.text     "content"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "report_entries", :force => true do |t|
    t.integer "report_section_id", :limit => 11
    t.text    "content"
    t.string  "entry_type"
    t.integer "display_order",     :limit => 11
  end

  create_table "report_entry_choices", :force => true do |t|
    t.integer "report_entry_id", :limit => 11
    t.text    "content"
    t.integer "display_order",   :limit => 11
  end

  create_table "report_restrictions", :force => true do |t|
    t.integer  "restrictable_id",   :limit => 11
    t.string   "restrictable_type"
    t.integer  "limitation_id",     :limit => 11
    t.string   "limitation_type"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "report_sections", :force => true do |t|
    t.integer "report_id",     :limit => 11
    t.string  "title"
    t.text    "description"
    t.integer "display_order", :limit => 11
  end

  create_table "report_submission_entries", :force => true do |t|
    t.integer  "report_submission_id",   :limit => 11
    t.integer  "report_entry_id",        :limit => 11
    t.datetime "datetime_value"
    t.integer  "integer_value",          :limit => 11
    t.float    "float_value"
    t.text     "text_value"
    t.string   "string_value"
    t.integer  "report_entry_choice_id", :limit => 11
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "user_id",                :limit => 11
    t.integer  "group_id",               :limit => 11
  end

  create_table "report_submissions", :force => true do |t|
    t.integer  "report_id",  :limit => 11
    t.integer  "user_id",    :limit => 11
    t.integer  "child_id",   :limit => 11
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "reports", :force => true do |t|
    t.string  "title"
    t.text    "description"
    t.integer "active",         :limit => 1
    t.integer "child_entry_id", :limit => 11
  end

  create_table "response_sets", :force => true do |t|
    t.integer  "user_id",       :limit => 11
    t.integer  "paper_form_id", :limit => 11
    t.datetime "completed_on"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "assignment_id", :limit => 11
  end

  create_table "responses", :force => true do |t|
    t.integer  "response_set_id", :limit => 11
    t.integer  "reply_id",        :limit => 11
    t.integer  "article_id",      :limit => 11
    t.integer  "integer_value",   :limit => 11
    t.float    "float_value"
    t.string   "string_value"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id",   :limit => 11
    t.string   "resource_type"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "roles_index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "roles_index_roles_on_name"

  create_table "text_message_users", :force => true do |t|
    t.integer  "text_message_id", :limit => 11
    t.integer  "user_id",         :limit => 11
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "text_messages", :force => true do |t|
    t.string   "content"
    t.integer  "user_id",    :limit => 11
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "user_imports", :force => true do |t|
    t.string   "file"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                :default => "", :null => false
    t.string   "encrypted_password",                   :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :limit => 11, :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "name"
    t.integer  "text_message_id",        :limit => 11
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "approved",               :limit => 1,  :default => 0
    t.string   "pin"
  end

  add_index "users", ["confirmation_token"], :name => "users_index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "users_index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "users_index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id", :limit => 11
    t.integer "role_id", :limit => 11
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "users_roles_index_users_roles_on_user_id_and_role_id"

end
