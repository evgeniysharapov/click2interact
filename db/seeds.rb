# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create! :name => 'Admin', :email => 'admin@click2interact.com', :password => '123456', :password_confirmation => '123456'
# override mass-assignment protection
user.update_attribute :confirmed_at, Time.now
user.add_role :admin

user_teacher = User.create! :name => 'John Teacher', :email => 'teacher@click2interact.com', :password => '123456', :password_confirmation => '123456'
# override mass-assignment protection
user_teacher.update_attribute :confirmed_at, Time.now
user_teacher.add_role :teacher

user_parent = User.create! :name => 'John Parent', :email => 'parent@click2interact.com', :password => '123456', :password_confirmation => '123456'
# override mass-assignment protection
user_parent.update_attribute :confirmed_at, Time.now
user_parent.add_role :parent

user_child = User.create! :name => 'John Child', :email => 'child@click2interact.com', :password => '123456', :password_confirmation => '123456'
# override mass-assignment protection
user_child.update_attribute :confirmed_at, Time.now
user_child.add_role :child


