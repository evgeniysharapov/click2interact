### UTILITY METHODS ###


# We create a Test User attributes
def test_user
  {email: "test@test.com", password: 'please', password_confirmation: 'please', name: 'Test User'}
end

def delete_user
  @user ||= User.first conditions: {:email => test_user[:email]}
  @user.destroy unless @user.nil?
end

def sign_up
   visit '/users/sign_up'
   fill_in "Email", :with => @visitor[:email]
   click_button "Sign Up"
end

def sign_in
  visit '/users/sign_in'
  fill_in "Email", :with => @visitor[:email]
  fill_in "Password", :with => @visitor[:password]
  click_button "Sign in"
end

### GIVEN ###
Given /^I am not logged in$/ do
  visit '/users/sign_out'
end

Given /^I am logged in$/ do
  @visitor = test_user
  sign_in
end

Given /^I exist as a user$/ do
  @user = FactoryGirl.create :user, test_user
end

Given /^I do not exist as a user$/ do
  delete_user
end

Given /^I exist as an unconfirmed user$/ do
  @user = FactoryGirl.create :user, test_user.merge(confirmed_at: nil)
end

### WHEN ###
When /^I sign in with valid credentials$/ do
  @visitor = test_user
  sign_in
end

When /^I sign out$/ do
  visit '/users/sign_out'
end

When /^I sign up with valid user data$/ do
  @visitor = test_user
  sign_up
end

When /^I sign up with an invalid email$/ do
  @visitor = test_user.merge(email: "notanemail")
  sign_up
end

When /^I return to the site$/ do
  visit '/'
end

When /^I sign in with a wrong email$/ do
  @visitor = test_user.merge(:email => "wrong@example.com")
  sign_in
end

When /^I sign in with a wrong password$/ do
  @visitor = test_user.merge(:password => "wrongpass")
  sign_in
end

When /^User already exists$/ do
  @user = FactoryGirl.create :user, test_user
end

When /^I fill in password and password confirmation$/ do
  fill_in "Password", with: test_user[:password]
  fill_in "Password confirmation", with: test_user[:password_confirmation]
end

When /^I click "(.*?)"$/ do |arg1|
  click_on arg1
end


### THEN ###
Then /^I should be signed in$/ do
  page.should have_content "Logout"
  page.should_not have_content /Sign up/i
  page.should_not have_content "Login"
end

Then /^I should be signed out$/ do
  page.should have_content /Sign Up/i
  page.should have_content "Login"
  page.should_not have_content "Logout"
end

Then /^I see an unconfirmed account message$/ do
  page.should have_content "You have to confirm your account before continuing."
end

Then /^I should see a message about confirmation link email$/ do
  # TODO: add div.flash_notice
  page.should have_content "A message with a confirmation link has been sent to your email address. Please open the link to activate your account."
end

Then /^I should see an error message that email has been taken$/ do
  find("div.error").should have_content("has already been taken")
end

Then /^I see a successful sign in message$/ do
  page.should have_content "Signed in successfully."
end

Then /^I should see a successful sign up message$/ do
  page.should have_content "Welcome! You have signed up successfully."
end

Then /^I should see an invalid email message$/ do
  page.should have_content "is invalid"
end

Then /^I should see a missing password message$/ do
  page.should have_content "Password can't be blank"
end

Then /^I should see a missing password confirmation message$/ do
  page.should have_content "Password doesn't match confirmation"
end

Then /^I should see a mismatched password message$/ do
  page.should have_content "Password doesn't match confirmation"
end

Then /^I should see a signed out message$/ do
  page.should have_content "Signed out successfully."
end

Then /^I see an invalid login message$/ do
  page.should have_content "Invalid email or password."
end

Then /^I should see my name$/ do
  @user = FactoryGirl.create :user, test_user
  page.should have_content @user[:name]
end

Then /^I should see "(.*?)"$/ do |arg1|
  page.should have_content arg1
end
