Feature: Sign up
  In order to get access to protected sections of the site
  As a user
  I want to be able to sign up

    Background:
      Given I am not logged in

    Scenario: User signs up with valid data
      When I sign up with valid user data
      Then I should see a message about confirmation link email

    Scenario: User signs up with already existing email
      When User already exists
      When I sign up with valid user data
      Then I should see an error message that email has been taken

    Scenario: User signs up with invalid email
      When I sign up with an invalid email
      Then I should see an invalid email message

    # TODO: we have two step sign up - admin has to approve user and only then he will get an email.
    # Scenario: A new person signs up
    #   When I sign up with valid user data
    #   And I should receive an email
    #   When I open the email
    #   Then I should see "confirm" in the email body
    #   When I follow "confirm" in the email
    #   Then I should see "You're almost done!"
    #   And I should see "Now create a password to securely access your account"
    #   When I fill in password and password confirmation
    #   And I click "Update User"
    #   Then I should see "Your account was successfully confirmed. You are now signed in."
