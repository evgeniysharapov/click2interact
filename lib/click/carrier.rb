# -*- coding: utf-8 -*-
require 'nokogiri'
require 'open-uri'

module Click
  module Carrier
    CARRIERS = [:att, :verizon, :sprint, :tmobile]
    CARRIERS_NAMES = {:att => 'AT&T', :verizon => "Verizon", :sprint => "Sprint", :tmobile => "T-Mobile"}
    PHONE_NUMBER_REGEX = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})/

    # return carrier information
    def find_carrier(phone)
      begin
        doc = Nokogiri::HTML(open(construct_url(phone)))
        description = doc.xpath('//div[@class="phone-header-text"]//span[@class="description"]/text()') do | desc |
          desc.text
        end
        case
        when description.text.match(/[Vv]erizon/)
          :verizon
        when description.text.match(/AT/)
          :att
        when description.text.match(/[Ss]print/)
          :sprint
        when description.text.match(/[Tt]\.*mobile/)
          :tmobile
        when description.text.match(/landline/)
          raise "Not a cellular phone number"
        end
      rescue e
        raise "Could not the phone line carrier"
      end
    end

    # returns either valid phone nubmer as string of digits, or
    # nil if phone is not valid
    def valid_phone_number(phone)
      if phone =~ PHONE_NUMBER_REGEX
        pnumber = phone.tr("- ()+","")
        if pnumber =~ /^1\d+/
          "#{phone}"
        else
          "1#{phone}"
        end
      elsif
        nil
      end
    end

    # Creates URL 
    def construct_url ( phone )
      phone_number = valid_phone_number(phone)
      unless phone_number.nil?
        "http://www.411.com/phone/#{phone_number}"
      else
        raise "Cannot recognize the phone number"
      end
    end
  end
end
