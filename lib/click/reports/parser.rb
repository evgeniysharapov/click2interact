require 'erb'

%w(report report_section report_entry report_entry_choice report_restriction).each { |mod| require mod; }

module Click
  module Reports
    class ParserError < StandardError; end

    class Parser
      attr_accessor :parsed

      def initialize
      end

      def load (str)
        self.instance_eval str
      end

      def unparse ( report )
        template = ERB.new <<-EOF
report "<%= report.title %>" <% if report.description %>, description: "<%= report.description %>" <% end %> <%= print_restrictions(report) %> do
<% report.report_sections.each do |section| %>
  section <% if section.title %> "<%= section.title %>" <% end %>  <% if section.description %>, description: "<%= section.description %>" <% end %> <%= print_restrictions(section) %> do
    <% section.report_entries.each do |entry| %>
      <%= entry.entry_type %> "<%= entry.content %>" <%= print_use_as_child(report, entry) %> <%= print_restrictions(entry) %> <% if entry.entry_type == 'choice' %>, choices: <%= entry.report_entry_choices.map(&:content) %> <% end %>
    <% end %>
  end
<% end %>
end
        EOF
        template.result(binding)
      end

      def report (title, opts = {}, &block)
        self.parsed = Report.new title: title, description: opts[:description]
        if block_given?
          self.instance_eval &block
        end
      end

      def section (title = nil, opts ={}, &block)
        order = self.parsed.report_sections.size
        rs = ReportSection.new title: title, description: opts[:description], display_order: order
        add_restrictions rs, opts
        self.parsed.report_sections << rs
        if block_given?
          self.instance_eval &block
        end
      end

      # creates parsers for entries types on the fly
      %w(date datetime time integer float text string choice user group).each do |type|
        define_method(type) do |content, opts = {}|
          order = self.parsed.report_sections.last.report_entries.size
          re = ReportEntry.new content: content, entry_type: type, display_order: order

          add_restrictions re, opts

          case type
          when 'choice'
            choices = opts[:choices] || []
            choices.each do |choice|
              order = re.report_entry_choices.size
              rec = ReportEntryChoice.new content: choice, display_order: order
              re.report_entry_choices << rec
            end
          when 'user'
            # this user entry will be used as a child_entry for the report
            if opts[:use_as] == :child
              self.parsed.child_entry = re
            end
          # when 'group'
          #   puts "Found Group"
          end
          self.parsed.report_sections.last.report_entries << re
        end
      end

      private
      # Checks given opts for the restricted_to symbol and then adds
      # appropriate {restrictions ReportRestriction} to the obj
      def add_restrictions(obj, opts = {})
        # can restrictionsbe added
        if obj.respond_to? :restrictions
          if opts[:restricted_to]
            case
            when opts[:restricted_to].is_a?(Symbol)
              # it is a role by default
              obj.restrictions << ReportRestriction.new(restrictable: obj, limitation: Role.find_by_name(opts[:restricted_to]))
            when opts[:restricted_to].is_a?(Hash)
              groups = opts[:restricted_to][:group] || opts[:restricted_to][:groups] || []
              roles = opts[:restricted_to][:role] || opts[:restricted_to][:roles] || []

              groups = [groups] unless groups.respond_to? :each
              roles = [roles] unless roles.respond_to? :each

              groups.each do |g|
                obj.restrictions << ReportRestriction.new(restrictable: obj, limitation: Group.find_by_name(g))
              end
              roles.each do |r|
                obj.restrictions << ReportRestriction.new(restrictable: obj, limitation: Role.find_by_name(r))
              end
            end
          end
        end
      end
      # unparses restrictions of the given object
      def print_restrictions(obj)
        str = nil
        if obj.respond_to? :restrictions
          # working the case when it looks like:
          # restricted_to: :admin
          # or
          # restricted_to: { group: :Test }
          if obj.restrictions.size == 1
            if obj.restrictions.first.limitation.is_a? Role
              str = "restricted_to: :#{obj.restrictions.first.limitation.name}"
            elsif obj.restrictions.first.limitation.is_a? Group
              str = "restricted_to: {group: :#{obj.restrictions.first.limitation.name}}"
            end
          elsif obj.restrictions.size > 1
            restr = {}
            {role: Role, group: Group}.each_pair do |symb, cls|
              obj.restrictions.each do |r|
                if r.limitation.is_a? cls
                  if restr.has_key?((symb.to_s + "s").to_sym) # :groups or :roles
                    # simply add whatever role or group is there
                    restr[(symb.to_s + "s").to_sym] << r.name.to_sym
                  elsif restr.has_key?(symb) # :group or :role
                    # already has a group or a role restriction, convert it to groups or roles
                    restr[(symb.to_s + "s").to_sym] = [restr[symb]]
                  else
                    # there's no group or role or roles key yet
                    restr[symb] = r.name.to_sym # :role
                  end
                end
              end
            end
            # print the result
            str = "restricted_to: #{restr}"
          end
        end
        # result of the print_restrictions
        if str
          ", #{str}"
        else
          ""
        end
      end

      # Prints use_as: :child for the entry if it is used as a child entry for the report
      def print_use_as_child(report, entry)
        if report.child_entry == entry
          ", use_as: :child"
        else
          ""
        end
      end

    end
  end
end
