module Click
  module Sendable
    extend ActiveSupport::Concern

    RECIPIENT_TYPES = %w(User Group)

    included do
      attr_accessible :sender, :recipient
      belongs_to :sender, class_name: 'User'
      belongs_to :recipient, class_name: 'User'
      validates_presence_of :sender, :recipient
    end


    module ClassMethods
      # Creates list of instances of the ActiveRecord for
      # a user (default behaviour) or a group of users depending
      # on parameter +:recipient_type+. +Id+ of the recipient is passed
      # in the recipient.
      # Block is used to work with each record before it's saved.
      # Returns list of created and saved models.
      # TODO: make that returning of the recipient_type in the records in a more elegant manner
      def create_for (*attributes, &blk)
        opts = attributes.extract_options!
        opts[:recipient_type] ||= 'User'
        records = []
        if opts[:recipient_type] == 'User'  # create single instance
          opts[:recipient] = User.find opts[:recipient]
          record = self.new opts.except(:recipient_type)
          # if there's any action execute it first
          if block_given?
            blk.call(record)
          end
          record.save
          records << record
          # we add a method returning recipient type to the result
          def records.recipient_type; 'User'; end

        elsif opts[:recipient_type] == 'Group' # create bunch of models
          # find a group
          group = Group.find opts[:recipient]
          self.transaction do
            group.users.each do |user|
              opts[:recipient] = user
              record = self.new opts.except(:recipient_type)
              # if there's any action execute it first
              if block_given?
                blk.call(record)
              end
              record.save
              records << record
            end
          end
          def records.recipient_type; 'Group'; end
        end
        records
      end

    end
  end
end

#ActiveRecord::Base.send(:include, Click::GroupCreatable)
