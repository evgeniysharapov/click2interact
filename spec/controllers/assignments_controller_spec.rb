# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
require 'spec_helper'

describe AssignmentsController do
  before :each do
    @sender = FactoryGirl.create :multiple_user
    @sender.confirm!
    @recipient = FactoryGirl.create :multiple_user
    @recipient.confirm!
    @paper_form = FactoryGirl.create :paper_form
    @sender.add_role :admin
    sign_in  @sender
  end

  it "creates an assignment for user" do
    expect {
    post :create, HashWithIndifferentAccess.new({ "utf8"=>"✓",
      "assignment"=>{"recipient_type"=>"User",
        "recipient"=>"1",
        "due_date"=>"2012-10-12"},
      "commit"=>"Create Assignment",
      "paper_form_id"=>"1"})
    }.to change(Assignment, :count).by(1)
    response.should redirect_to paper_form_path(1)
   end

end
