require 'spec_helper'

describe HomeController do
  describe "GET 'index' as a logged in user" do
    before do
      @user = FactoryGirl.create :user
      @user.confirm!
      sign_in @user
    end
    it "should be successful" do
      get 'index'
      response.should be_success
    end
  end

end
