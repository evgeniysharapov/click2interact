require 'spec_helper'

describe PaperFormsController do

  context "for authenticated user" do
    before :each do
      @user = FactoryGirl.create :user
      @user.confirm!
      @user.add_role :admin
      sign_in @user
      @attr = {"title"=>"This is a simple paper form", "articles_attributes"=>{"0"=>{"content"=>"Ok, what's the deal with the Johnsons? ", "response_type"=>"string", "_destroy"=>""}}}
    end

    it "should get index" do
      get :index
      response.should be_success
    end

    it "POST /paper_forms" do
      lambda do
        post :create, :paper_form => @attr
      end.should change(PaperForm, :count).by(1)
    end

    it "POST /paper_forms" do
      post :create, :paper_form => @attr
      flash[:notice].should_not be_nil
    end
  end

  context "Not authenticated user" do
    it "redirects it to login page" do
      get :index
      response.status.should be(302)
    end
  end

end
