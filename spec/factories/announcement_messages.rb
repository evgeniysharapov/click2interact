# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :announcement_message do
    content "MyText announcement"
    association :author, factory: :multiple_user

    factory :announcement_message_announced do
      after(:build) do |am|
        am.announce_to(am.author)
      end
    end
  end
end
