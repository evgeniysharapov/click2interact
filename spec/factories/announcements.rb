# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :announcement do
    association :sender, factory: :multiple_user
    association :recipient, factory: :multiple_user
    showable false
    after(:build) do |a|
      a.message = FactoryGirl.build(:announcement_message)
      a.message.announcements << a
    end
  end
end
