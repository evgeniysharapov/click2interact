# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :answer_value do
    user nil
    question nil
    answer nil
    assignment nil
  end
end
