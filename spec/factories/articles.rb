# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    content "What is your weight?"
    association :paper_form, factory: :paper_form
  end
end
