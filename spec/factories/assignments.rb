# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :assignment do
    association :sender, factory: :multiple_user
    association :recipient, factory: :multiple_user
    due_date "2012-09-18"
  end
end
