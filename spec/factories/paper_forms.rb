# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :paper_form do
    title "Paper Form"
    association :author, factory: :multiple_user
  end
end
