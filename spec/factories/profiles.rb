# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :profile do
    phone "16307793208"
    carrier :att
    association :user, :factory => :user
  end

end
