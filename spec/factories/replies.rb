# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :reply do
    association :article, factory: :article
    content "MyText"
  end
end
