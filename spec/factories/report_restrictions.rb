# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :report_restriction do
    restrictable ""
    limitation ""
  end
end
