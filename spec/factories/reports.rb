# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :report do
    title "MyString"
    description "MyText"
    active false
  end

  factory :report_section do
    title "Section Title"
    sequence(:display_order)
  end

  factory :report_entry do
    content "Entry"
    entry_type "string"
    sequence(:display_order)
  end

  factory :report_entry_choice do
    content "Report Entry Choice"
    sequence(:display_order)
  end

end
