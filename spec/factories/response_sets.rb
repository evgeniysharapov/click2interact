# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :response_set do
    association :user, factory: :user
    association :paper_form, factory: :paper_form
    association :assignment, factory: :assignment
  end
end
