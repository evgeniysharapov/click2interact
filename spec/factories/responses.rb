# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :response do
    association :article, factory: :article
    association :reply, factory: :reply
    association :response_set, factory: :response_set
  end
end
