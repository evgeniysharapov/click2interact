# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :user do
    name 'Test User'
    email 'example@example.com'
    password 'please'
    password_confirmation 'please'
    confirmed_at Time.now
  end

  sequence(:seq_name) { |n| "User #{n}" }
  sequence(:seq_email) { |n| "user#{n}@email#{n}.com" }

  factory :multiple_user, class: User  do
    name { generate :seq_name }
    email { generate :seq_email }
    password 'please'
    password_confirmation 'please'
  end

end
