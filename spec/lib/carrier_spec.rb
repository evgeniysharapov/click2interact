require 'spec_helper'
require 'click/carrier'

describe Click::Carrier do
  include Click::Carrier
  it "finds a correct provider" do
    find_carrier("630-779-3208").should == :verizon
  end
end
