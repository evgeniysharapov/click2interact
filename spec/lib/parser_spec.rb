require 'spec_helper'
require 'click/reports/parser'

describe Click::Reports::Parser do
  before(:all) do
    @group1 = FactoryGirl.create :group, name: "Test"
    @group2 = FactoryGirl.create :group, name: "Group 1"
    @role1 = FactoryGirl.create :role, name: :admin
    @role2 = FactoryGirl.create :role, name: :teacher

    @parser = Click::Reports::Parser.new
    @parser.load <<EOD
report "This is a report" do
  section "This is a section" do
    string "String entry"
    text "Text entry"
  end

  section "This is a second section" do
    integer "Integer number", restricted_to: :#{@role1.name}
    float "Float number", restricted_to: { group: :#{@group1.name} }
    choice "How happy are you?", choices: ["very", "very very", "very very very"]
  end

  section "Test User and Group", restricted_to: {roles: ['#{@role1.name}', :#{@role2.name}] } do
    user "Child", restricted_to: {groups: ["#{@group1.name}", "#{@group2.name}"]}
    group "User Group"
  end

end
EOD
  end

  describe "Parsed document"  do
    subject { @parser.parsed }
    it { should be_a(Report) }
    it "unparses into a document" do
      new_parser = Click::Reports::Parser.new
      r = Report.new title: 'Report'
      r.report_sections << ReportSection.new(title: 'Section')
      r.report_sections.last.report_entries << ReportEntry.new(content: "Entry", entry_type: 'string')
      #puts new_parser.unparse(r)
    end
  end

  describe "Parsed Sections" do
    subject {@parser.parsed.report_sections}
    it { should have(3).items }


    describe "First Section" do
      subject {@parser.parsed.report_sections.first}
      it { should be_a(ReportSection)}
      its(:title) { should == "This is a section"}
      its(:description) { should be_nil}
      its(:display_order) { should == 0}

      describe "Entries" do
        subject {@parser.parsed.report_sections.first.report_entries}
        it { should have(2).items}
        describe "String" do
          subject { @parser.parsed.report_sections.first.report_entries.first }
          it { should be_a(ReportEntry)}
          its(:entry_type) {should == "string"}
          its(:content) { should == "String entry" }
          its(:report_entry_choices) { should be_empty }
        end
        describe "Text" do
          subject { @parser.parsed.report_sections.first.report_entries.second }
          it { should be_a(ReportEntry)}
          its(:entry_type) {should == "text"}
          its(:content) { should == "Text entry" }
          its(:report_entry_choices) { should be_empty }
        end
      end

    end

    describe "Second Section" do
      subject {@parser.parsed.report_sections.second}
      it { should be_a(ReportSection)}
      its(:title) { should == "This is a second section"}
      its(:description) { should be_nil}
      its(:display_order) { should == 1}

      describe "Entries" do
        subject {@parser.parsed.report_sections.second.report_entries}
        it { should have(3).items}
        describe "Integer" do
          subject { @parser.parsed.report_sections.second.report_entries.first }
          it { should be_a(ReportEntry)}
          its(:entry_type) {should == "integer"}
          its(:content) { should == "Integer number" }
          its(:report_entry_choices) { should be_empty }
        end
        describe "Float" do
          subject { @parser.parsed.report_sections.second.report_entries.second }
          it { should be_a(ReportEntry)}
          its(:entry_type) {should == "float"}
          its(:content) { should == "Float number" }
          its(:report_entry_choices) { should be_empty }
        end
        describe "Choice" do
          subject { @parser.parsed.report_sections.second.report_entries.third }
          it { should be_a(ReportEntry)}
          its(:entry_type) {should == "choice"}
          its(:content) { should == "How happy are you?" }
          its(:report_entry_choices) { should_not be_nil }
          its(:report_entry_choices) { should be_a(Array) }
          its(:report_entry_choices) { should have(3).items }
          it "should be array of choices" do
            subject.report_entry_choices.each do |choice|
              choice.should be_a(ReportEntryChoice)
            end
          end
          its("report_entry_choices.first.content"){ should == "very"}
          its("report_entry_choices.second.content"){ should == "very very"}
          its("report_entry_choices.third.content"){ should == "very very very"}
          its("report_entry_choices.first.display_order"){ should == 0}
          its("report_entry_choices.second.display_order"){ should == 1}
          its("report_entry_choices.third.display_order"){ should == 2}

        end
      end

    end

    describe "Third Section" do
      subject {@parser.parsed.report_sections.third}
      it { should be_a(ReportSection)}
      its(:title) { should == "Test User and Group"}
      its(:description) { should be_nil}
      its(:display_order) { should == 2}
      its(:restrictions){ should have(2).items }
      describe "User" do
        subject{@parser.parsed.report_sections.third.report_entries.first}
        it { should be_a(ReportEntry) }
        its(:entry_type) {should == "user"}
        its(:content) { should == "Child" }
        describe "Child" do
          subject { @parser.parsed.report_sections.third.report_entries.first.restrictions }
          its(:size) {should == 2}
          describe "First" do
            subject { @parser.parsed.report_sections.third.report_entries.first.restrictions.first }
            it {should be_a(ReportRestriction)}
            its(:restrictable) { should be_a(ReportEntry)}
            its(:limitation) { should be_a(Group)}
            its(:limitation) { should eq(@group1)}
          end
          describe "Second" do
            subject { @parser.parsed.report_sections.third.report_entries.first.restrictions.second }
            it {should be_a(ReportRestriction)}
            its(:restrictable) { should be_a(ReportEntry)}
            its(:limitation) { should be_a(Group)}
            its(:limitation) { should eq(@group2)}
          end
        end
      end
    end

    describe "Unparsed document" do
      before(:all) do
         @section3 = <<EOD
  section "Test User and Group", restricted_to: {roles: ['#{@role1.name}', :#{@role2.name}] } do
    user "Child", restricted_to: {groups: ["#{@group1.name}", "#{@group2.name}"]}
    group "User Group"
  end
EOD
      end
      
    end

  end
end
