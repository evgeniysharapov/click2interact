require "spec_helper"

describe TextMessageMailer do
  context "#text_message_email" do
    let (:sender_name) {"Evgeniy"}
    let (:carrier) {"AT&T"}
    let (:phone) {"6307793208"}
    let (:content) {"This is a content"}
    let (:msg) { TextMessageMailer.text_message_email(sender_name, phone,carrier,content)}

    it "emails to carriers" do
      msg.to.should == ["6307793208@txt.att.net"]
    end

    it "throws an error if no carrier was found" do
      lambda{ TextMessageMailer.text_message_email(sender_name, phone, "Unknown", content) }.should raise_error
    end

    it "returns mail object with content" do
      msg.body.encoded.should match(content)
    end

    it "has a from email address" do
      msg.from.should == ["notifications@click2interact.com"]
    end
  end
end
