require 'spec_helper'

describe AnnouncementMessage do
  context "Validations" do
    it "We create valid instances" do
      a = FactoryGirl.build :announcement_message_announced
      a.should be_valid
    end

    it "is invalid without author" do
      a = FactoryGirl.build :announcement_message_announced, author: nil
      a.should_not be_valid
    end

    it "is invalid without content" do
      a = FactoryGirl.build :announcement_message_announced, content: nil
      a.should_not be_valid
    end
  end

  context "in a system" do
    before :each do
      @author = FactoryGirl.create :multiple_user
      @group = FactoryGirl.create :group
      (1..3).each do |n|
        @group.users << FactoryGirl.create(:multiple_user)
      end
      @ann = FactoryGirl.build(:announcement_message)
    end

    context "con be announced to a user" do
      before :each do
        @ann.announce_to @group.users.first
      end
      it "creates 1 announcement" do
        @ann.announcements.size.should == 1
      end
    end
    context "can be announced to a group" do
      before :each do
        @ann.announce_to @group
      end
      it "creates # announcement equal to number of users in a group" do
        @ann.announcements.size.should == @group.users.size
      end
    end
  end
end
