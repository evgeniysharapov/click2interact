# == Schema Information
#
# Table name: announcements
#
#  id           :integer          not null, primary key
#  content      :text
#  recipient_id :integer
#  sender_id    :integer
#  showable     :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'spec_helper'

describe Announcement do

  describe "validations" do
    it "is valid by default" do
      a = FactoryGirl.build :announcement
      a.should be_valid
    end

    it "is invalid without sender" do
      a = FactoryGirl.build :announcement, sender: nil
      a.should_not be_valid
    end

    it "is invalid without recipient" do
      a = FactoryGirl.build :announcement, recipient: nil
      a.should_not be_valid
    end

    it "is invalid without message" do
      a = FactoryGirl.build :announcement
      # because message is set in after_build trigger in FG
      a.message = nil
      a.should_not be_valid
    end
  end
end
