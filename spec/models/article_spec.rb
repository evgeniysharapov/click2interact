# == Schema Information
#
# Table name: articles
#
#  id            :integer          not null, primary key
#  paper_form_id :integer
#  content       :text
#  response_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'spec_helper'

describe Article do

  before :each do
    @art = FactoryGirl.create :article
  end

  it "should be valid" do
    @art.should be_valid
  end

end
