# == Schema Information
#
# Table name: assignments
#
#  id              :integer          not null, primary key
#  sender_id       :integer
#  recipient_id    :integer
#  due_date        :date
#  completed_at    :datetime
#  assignable_id   :integer
#  assignable_type :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'spec_helper'

describe Assignment do

  describe "validations" do

    it "is invalid without sender" do
      a = FactoryGirl.build :assignment, sender: nil
      a.should_not be_valid
    end

    it "is invalid without recipient" do
      a = FactoryGirl.build :assignment, recipient: nil
      a.should_not be_valid
    end

    it "is invalid without due date" do
      a = FactoryGirl.build :assignment, due_date: nil
      a.should_not be_valid
    end
  end

  describe "Created" do
    before :each do
      @sender = FactoryGirl.create :multiple_user
      @group = FactoryGirl.create :group
      # add users into the group
      (1..3).each do |n|
        @group.users << FactoryGirl.create(:multiple_user)
      end
      @form = FactoryGirl.create :paper_form
    end

    describe "For User" do
      before :each do
        @assignments = Assignment.create_for sender: @sender, recipient: @group.users.first, recipient_type: "User", due_date: Time.now, assignable: @form
      end
      it "creates a list of assignments" do
        @assignments.count.should == 1
      end
      it "adds an assignment for a user" do
        Assignment.where(recipient_id: @group.users.first).count.should == 1
      end
      it "creates valid assignments" do
        @assignments.map(&:valid?).should =~ [true]
      end
    end

    describe "For Group" do
      before :each do
        @assignments = Assignment.create_for sender: @sender, recipient: @group, recipient_type: "Group", due_date: Time.now, assignable: @form
      end

      it "creates a list of assignments" do
        @assignments.count.should == 3
      end

      it "adds one assignment for each member of the group" do
        @assignments.map(&:recipient).should =~ @group.users
      end

      it "creates valid assignments" do
        @assignments.map(&:valid?).should =~ [true]*3
      end
    end
  end

end
