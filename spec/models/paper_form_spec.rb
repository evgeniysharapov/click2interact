# == Schema Information
#
# Table name: paper_forms
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  author_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe PaperForm do

  before :each do
    @paper_form = FactoryGirl.create :paper_form
  end

  it "has articles" do
    @paper_form.should respond_to(:articles)
  end

  it "should be valid" do
    @paper_form.should be_valid
  end

end
