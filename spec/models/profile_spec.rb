# == Schema Information
#
# Table name: profiles
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  phone      :string(255)
#  carrier    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Profile do
    before :each do
      @user = FactoryGirl.create :user
      @profile = FactoryGirl.create :profile, :user => @user
    end

  it "should always belong to a user" do
    @profile.user.should_not be_nil
  end

  context "having phone" do
    it "has a valid factory" do
      @profile.should be_valid
    end
    it "should should have 10 or 11 digits" do
      @profile.phone.length.should >= 10
      @profile.phone.length.should <= 11
    end
    it "should be one of the carriers" do
      Profile::CARRIERS.should include(@profile.carrier)
    end

# phone number is not necessarily unique
#    it "has unique phone number" do
#      user2 = FactoryGirl.create :user, :email => 'test@test.com', :password => 'password', :password_confirmation => 'password'
#      profile2 = FactoryGirl.build :profile, :user => user2
#      profile2.should_not be_valid
#    end
  end
end
