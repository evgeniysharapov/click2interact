# == Schema Information
#
# Table name: replies
#
#  id         :integer          not null, primary key
#  article_id :integer
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Reply do

  before :each do
    @reply = FactoryGirl.create :reply
  end

  it "should be valid" do
    @reply.should be_valid
  end

end
