require 'spec_helper'

describe ReportEntry do

  before(:each) do
    @group1 = FactoryGirl.create :group, name: "Test"
    @group2 = FactoryGirl.create :group, name: "Group 1"
    @role1 = FactoryGirl.create :role, name: :admin
    @role2 = FactoryGirl.create :role, name: :teacher

    @entry = FactoryGirl.create :report_entry, entry_type: 'string'

    @entry.restrictions << ReportRestriction.new(restrictable: @entry, limitation: @group1)
    @entry.restrictions << ReportRestriction.new(restrictable: @entry, limitation: @group2)
    @entry.restrictions << ReportRestriction.new(restrictable: @entry, limitation: @role1)
    @entry.restrictions << ReportRestriction.new(restrictable: @entry, limitation: @role2)

    @entry.save

  end

  subject { @entry }
  it{should be_valid}
  it "restrictions size" do
    r = @entry.restrictions
    r.size.should == 4
  end
  its('role_restrictions.size') {should == 2}
  its('group_restrictions.size') {should == 2}

end
