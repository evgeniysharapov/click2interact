require 'spec_helper'

describe Report do
  before(:each) do
    @report = FactoryGirl.build :report
  end

  describe @report do
    it {should be_valid}
  end

  describe "Typical report" do
    before (:each) do
      report_entry = FactoryGirl.build :report_entry
      report_section = FactoryGirl.build :report_section
      report_section.report_entries << report_entry
      @report.report_sections << report_section
    end

    describe "report" do
      subject { @report }
      it { should be_valid }
      its("report_sections.size") {should eq(1)}
    end
  end

end
