# -*- coding: utf-8 -*-
require 'spec_helper'

describe ReportSubmission do
  before(:all) do
    @report = FactoryGirl.create :report
    @section_1 = FactoryGirl.create :report_section, title: "Section 1"
    @report_entry_1 = FactoryGirl.create :report_entry, entry_type: 'string'
    @report_entry_2 = FactoryGirl.create :report_entry, entry_type: "integer"
    @section_1.report_entries << @report_entry_1
    @section_1.report_entries << @report_entry_2

    @section_2 = FactoryGirl.create :report_section, title: "Section 2"
    @report_entry_3 = FactoryGirl.create :report_entry, entry_type: "user"
    @report_entry_4 = FactoryGirl.create :report_entry, entry_type: "group"
    @section_2.report_entries << @report_entry_3
    @section_2.report_entries << @report_entry_4

    @report.report_sections << @section_1
    @report.report_sections << @section_2

    @report.save

    @group = FactoryGirl.create :group
    @user = FactoryGirl.create :user
  end

  describe "Freshly created ReportSubmission" do
    subject { report_submission }

    context  "no author, no report" do
      let(:report_submission) { ReportSubmission.new().organize_submission_entries }
      it { should_not be_valid}
    end

    context "with report" do
      let(:report_submission) { ReportSubmission.new(report: @report).organize_submission_entries }
      it { should_not be_valid}
    end

    context "with user" do
      let(:report_submission) { ReportSubmission.new(user: @user).organize_submission_entries }
      it { should_not be_valid }
    end

    context "with report and user" do
      let(:report_submission) do
        ReportSubmission.new(user: @user, report: @report).organize_submission_entries
      end

      it { should be_valid }
      it { subject.sections.size.should == @report.report_sections.size }
      it { subject.report_submission_entries.size.should == @report.report_sections.map(&:report_entries).flatten.size }

      context "first section" do
        subject { report_submission.sections.first }
        its(:section) { should eq(@report.report_sections.first) }
        its("entries.size") { should == @report.report_sections.first.report_entries.size }
      end

      context "second section" do
        subject { report_submission.sections.second }
        its(:section) { should eq(@report.report_sections.second) }
        its("entries.size") { should == @report.report_sections.second.report_entries.size }
      end
    end
  end

  describe "add submission values" do
    subject { @report_submission_1 }

    before(:all) do
      @report_submission_1 = ReportSubmission.new user: @user, report: @report
      @report_submission_1.organize_submission_entries
      @report_submission_1.report_submission_entries[0].string_value = "Test"
      @report_submission_1.report_submission_entries[1].integer_value = 1
      @report_submission_1.report_submission_entries[2].user_value = @user
      @report_submission_1.report_submission_entries[3].group_value = @group
    end

    it { should be_valid }

    describe "child pointing entry" do
      before(:each) do
        @report.child_entry = @report_entry_3
        @report.save
        @report_submission_1.save
      end
      its(:child) { should == @user}
    end

  end

  describe "massive assignment" do
    before(:all) do
      attrs = HashWithIndifferentAccess.new
      attrs.merge!({"report_submission"=>{"report_submission_entries_attributes"=>{
            "0"=>{"report_entry_id"=>@report_entry_1.id.to_s, "string_value"=>"fasdfasdfasd"},
            "1"=>{"report_entry_id"=>@report_entry_2.id.to_s, "text_value"=>"adsfasdf"},
            "2"=>{"report_entry_id"=>@report_entry_3.id.to_s, "user_id"=>@user.id.to_s},
            "3"=>{"report_entry_id"=>@report_entry_4.id.to_s, "group_id"=>@group.id.to_s}}}})
      @report_submission = ReportSubmission.new( attrs[:report_submission])
      @report_submission.user = @user
      @report_submission.report = @report
      @report_submission.save!
    end

    it "has to be valid" do
      @report_submission.should be_valid
    end
    it "should have no error messages" do
      @report_submission.errors.to_a.should == []
    end

  end


end
