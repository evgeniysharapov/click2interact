# == Schema Information
#
# Table name: response_sets
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  paper_form_id :integer
#  completed_on  :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  assignment_id :integer
#

require 'spec_helper'

describe ResponseSet do
  before :each do
    @rs = FactoryGirl.create :response_set
  end

  it "should be valid" do
    @rs.should be_valid
  end

end
