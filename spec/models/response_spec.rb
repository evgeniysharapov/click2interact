# == Schema Information
#
# Table name: responses
#
#  id              :integer          not null, primary key
#  response_set_id :integer
#  reply_id        :integer
#  article_id      :integer
#  integer_value   :integer
#  float_value     :float
#  string_value    :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'spec_helper'

describe Response do
  before :each do
    @resp = FactoryGirl.create :response
  end

  it "should be valid" do
    @resp.should be_valid
  end
end
