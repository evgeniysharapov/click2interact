require 'spec_helper'

describe Click::Sendable do

  shared_examples "sendable model of" do |model|
    it { model.should respond_to(:create_for) }
  end

  shared_examples "sendable model instance of" do
    it { instance.should respond_to(:sender)}
    it { instance.should respond_to(:recipient)}
  end

  describe Assignment do
    include_examples "sendable model of", Assignment
    it_behaves_like "sendable model instance of" do
      let(:instance) {FactoryGirl.create :assignment}
    end
  end

  describe Announcement do
    include_examples "sendable model of", Announcement
    it_behaves_like "sendable model instance of" do
      let(:instance) {FactoryGirl.create :announcement}
    end
  end

end
