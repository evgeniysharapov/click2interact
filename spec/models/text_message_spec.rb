# == Schema Information
#
# Table name: text_messages
#
#  id         :integer          not null, primary key
#  content    :string(255)
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe TextMessage do
  before :each do
    @u1 = FactoryGirl.create :multiple_user
    @u2 = FactoryGirl.create :multiple_user
    @u3 = FactoryGirl.create :multiple_user
    @prof = FactoryGirl.create :profile
  end

  it "#from creates a TextMessage with a sender" do
    m = TextMessage.from @u1 
    m.sender.should == @u1
  end

  it "has can be added to a recipient" do
    m = @u1.sms_to_receive.create :content => "test"
    m.content.should == "test"
    m.recipients.size.should be(1)
    m.recipients.first.should == @u1
  end

  it "can be used to find recipients of the message" do
    m = @u1.sms_to_receive.create :content => "test"
    User.recipients_of(m).should =~ m.recipients
  end

  it "has a content that is no more than 140 chars"


  it "has to send an email" do
    m = @u1.sms_to_send.create :content => "test"
    m.recipients = [@prof.user]
    m.send_text

    ActionMailer::Base.deliveries.should_not be_empty
  end

end
