require 'spec_helper'

describe User do

  it "should require an email address" do
    no_email_user = FactoryGirl.build :user, email: ""
    no_email_user.should_not be_valid
  end

  it "should accept valid email addresses" do
    addresses = %w[user@foo.com THE_USER@foo.bar.org first.last@foo.jp]
    addresses.each do |address|
      valid_email_user = FactoryGirl.build :user, email: address
      valid_email_user.should be_valid
    end
  end

  it "should reject invalid email addresses" do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
    addresses.each do |address|
      invalid_email_user = FactoryGirl.build :user, :email => address
      invalid_email_user.should_not be_valid
    end
  end

  it "should reject duplicate email addresses" do
    u = FactoryGirl.create :user
    email = u.email
    user_with_duplicate_email = FactoryGirl.build :user, email: email
    user_with_duplicate_email.should_not be_valid
  end

  it "should reject email addresses identical up to case" do
    u = FactoryGirl.create :user
    upcased_email = u.email.upcase
    user_with_duplicate_email = FactoryGirl.build :user, email: upcased_email
    user_with_duplicate_email.should_not be_valid
  end

  describe "passwords" do

    before(:each) do
      @user = User.new(@attr)
    end

    it "should have a password attribute" do
      @user.should respond_to(:password)
    end

    it "should have a password confirmation attribute" do
      @user.should respond_to(:password_confirmation)
    end
  end

  describe "password validations" do
    # it "should require a password" do
    #   u = FactoryGirl.build :user, :password => "", :password_confirmation => ""
    #   u.should_not be_valid
    # end

    # it "should require a matching password confirmation" do
    #   u = FactoryGirl.build :user, :password_confirmation => "invalid"
    #   u.should_not be_valid
    # end

    it "should reject short passwords" do
      short = "a" * 5
      u = FactoryGirl.build :user, :password => short, :password_confirmation => short
      u.should_not be_valid
    end
  end

  describe "password encryption" do

    before(:each) do
      @user = FactoryGirl.create :user
    end

    it "should have an encrypted password attribute" do
      @user.should respond_to(:encrypted_password)
    end

    it "should set the encrypted password attribute" do
      @user.encrypted_password.should_not be_blank
    end

  end

  describe "User's PIN" do
    before(:each) do
      @user = FactoryGirl.create :user
    end

    it "should be nil for new user" do
      @user.pin.should be_nil
    end

    it "should approve user if set" do
      @user.update_attributes pin: "1234"
      @user.should be_approved
    end
  end

end
