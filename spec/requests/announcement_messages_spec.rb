require 'spec_helper'

describe "AnnouncementMessages" do
  context "Not authenticated user" do
    describe "GET /announcements" do
      it "should be redirected away" do
        get announcement_messages_path
        response.status.should be(302)
      end
    end
  end
  context "Authenticated user" do
    before :each do
      # signing in
      @visitor = FactoryGirl.create :multiple_user
      @visitor.confirm!
      login_as @visitor, scope: :user
    end

    context "as admin" do
      before :each do
        @visitor.add_role :admin
      end

      describe "GET /announcements" do
        it "works" do
          get announcement_messages_path
          response.status.should be(200)
        end
      end
    end
  end
end
