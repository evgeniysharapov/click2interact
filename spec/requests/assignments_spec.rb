require 'spec_helper'

describe "Assignments" do
  before :each do
    @sender = FactoryGirl.create :multiple_user
    @sender.confirm!
    @recipient = FactoryGirl.create :multiple_user
    @recipient.confirm!
    @paper_form = FactoryGirl.create :paper_form
    @group = FactoryGirl.create :group
    @group.users << @recipient
    @group.users << @sender
    # signing in
    login_as @sender, scope: :user
  end

  context "User with admin role" do
    before :each do
      @sender.add_role :admin
      visit new_paper_form_assignment_path(@paper_form)
    end

    describe "User assignment" do
      # it "populates dropdown with users", js: true do
      #   choose "User"
      #   page.should have_selector("select > option[value='#{@sender.id}']")
      #   page.should have_content("#{@sender.name}")
      #   page.should have_selector("select > option[value='#{@recipient.id}']")
      #   page.should have_content("#{@recipient.name}")
      # end
      it "shows error if there's no due date", js: true do
        expect {
          choose "User"
          select("#{@recipient.name}", from: "Assign To")
          click_button("Create Assignment")
        }.to change(Assignment,:count).by(0)
        page.should have_content("can't be blank")
        page.should have_content("Please review the problems below:")
      end
      it "adds new user assignment", js: true do
        expect {
          choose "User"
          select("#{@recipient.name}", from: "Assign To")
          fill_in("assignment_due_date", with: Time.now.strftime("%Y-%d-%m"))
          # have to remove focus from datepicker
          find("h1").click
          click_button("Create Assignment")
        }.to change(Assignment,:count).by(1)
        page.should have_content("Assignment was successfully created.")
      end
    end

    describe "Group assignment" do
      # it "populates dropdown with groups", js: true do
      #   choose "Group"
      #   page.should have_selector("select > option[value='#{@group.id}']")
      #   page.should have_content("#{@group.name}")
      # end
      it "shows error if there's no due date", js: true do
        expect {
          choose "Group"
          select("#{@group.name}", from: "Assign To")
          click_button("Create Assignment")
        }.to change(Assignment,:count).by(0)
        page.should have_content("can't be blank")
        page.should have_content("Please review the problems below:")
      end
      it "adds new assignment for each group member", js: true do
        expect {
          choose "Group"
          select("#{@group.name}", from: "Assign To")
          fill_in("assignment_due_date", with: Time.now.strftime("%Y-%d-%m"))
          # have to remove focus from datepicker
          find("h1").click
          click_button("Create Assignment")
        }.to change(Assignment,:count).by(@group.users.count)
        page.should have_content("Assignments were successfully created.")
      end
    end
  end

end
