require 'spec_helper'


describe "Groups" do
  context "Not authenticated user" do
    describe "GET /groups" do
      it "should be redirected away" do
        get groups_path
        response.status.should be(302)
      end
    end
  end
  context "Authenticated user" do
    before :each do
      # signing in
      @visitor = FactoryGirl.create :multiple_user
      @visitor.confirm!
      login_as @visitor, scope: :user
    end

    context "as admin" do
      before :each do
        @visitor.add_role :admin
      end

      describe "GET /groups" do
        it "works" do
          get groups_path
          response.status.should be(200)
        end
      end
    end
  end
end
