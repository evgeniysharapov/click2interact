require 'spec_helper'

describe "PaperForms" do
  describe "GET /paper_forms" do

    context "Not authenticated user " do
      it "should be redirected away" do
        get paper_forms_path
        response.status.should be(302)
      end
    end
  end
end
