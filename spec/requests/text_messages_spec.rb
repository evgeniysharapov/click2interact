require 'spec_helper'

describe "TextMeessage" do
  context "Not authenticated user" do
    describe "GET /sms" do
      it "should be redirected away" do
        get text_messages_path
        response.status.should be(302)
      end
    end
  end
  context "Authenticated user" do
    before :each do
      # signing in
      @visitor = FactoryGirl.create :multiple_user
      @visitor.confirm!
      login_as @visitor, scope: :user
    end

    context "as admin" do
      before :each do
        @visitor.add_role :admin
      end

      describe "GET /sms" do
        it "works" do
          get text_messages_path
          response.status.should be(200)
        end
      end

      describe "POST /sms" do
      end
    end
  end
end
