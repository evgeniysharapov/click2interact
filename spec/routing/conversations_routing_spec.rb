require "spec_helper"

describe ConversationsController do
  describe "routing" do

    it "routes to #index" do
      get("/conversations").should route_to("conversations#index")
    end

    it "routes to #new" do
      get("/conversations/new").should route_to("conversations#new")
    end

    it "routes to #show" do
      get("/conversations/1").should route_to("conversations#show", :id => "1")
    end

  end
end
